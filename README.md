# JStatic

![Maven Central](https://img.shields.io/maven-central/v/com.mohamnag/jstatic)
![Sonatype Nexus (Snapshots)](https://img.shields.io/nexus/s/com.mohamnag/jstatic?server=https%3A%2F%2Foss.sonatype.org)
[![Netlify Status](https://api.netlify.com/api/v1/badges/15144be6-1976-4973-9d37-7e2d642dc349/deploy-status)](https://app.netlify.com/sites/jstatic/deploys)


Is a Java static file generator built from a bunch of plugins. It might be used to generate HTML and other web assets but has a wider possible use-case scenarios in mind as integrates very well into Java build tools and processes. For example it was used for generating customizable Java code out of OpenAPI specification files.

Refer to [official website](https://jstatic.naghavi.me) for more information and the documentation.
