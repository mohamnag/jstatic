/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static uk.org.webcompere.systemstubs.SystemStubs.withEnvironmentVariable;

@Slf4j
public class IntegrationTest {

  @RegisterExtension
  static WireMockExtension storyblokMock = WireMockExtension.newInstance()
    .options(WireMockConfiguration.options().port(8090))
    .build();
  private final Path outputPath = Paths.get("build/tmp/int-test/");

  @Test
  void fullRun() throws Exception {
    storyblokMock
      .stubFor(get(urlPathEqualTo("/stories"))
        .withQueryParam("token", equalTo("blablatoken"))
        .withQueryParam("version", equalTo("draft"))
        .withQueryParam("cv", matching("[0-9]+"))
        .willReturn(
          ok()
            .withBodyFile("storyblok.json")
        )
      );

    // given output dir is cleaned up and empty
    if (outputPath.toFile().exists()) {
      try (var pathStream = Files.walk(outputPath)) {
        var allDeleted = pathStream
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .map(File::delete)
          .reduce(true, (aBoolean, aBoolean2) -> aBoolean && aBoolean2);

        if (!allDeleted) {
          log.warn("Not all output files under {} could be deleted. Might see interference from old runs.", outputPath);
        }
      }
    }

    // and we have some env variables set
    withEnvironmentVariable("JSTATIC_DATA_ROOT__IS_PRODUCTION", "true")
      .execute(() -> {
        // when running the application
        Application.main(new String[]{"--config=src/test/resources/int-test/config.yaml"});
      });

    // then expect that resulting data is found on output
    Path index = outputPath.resolve("index.html");
    Path blogPost1 = outputPath.resolve("blog/post1.html");
    Path blogPost2 = outputPath.resolve("blog/post2.html");
    Path sampleYamlPage = outputPath.resolve("sample-yaml-page.html");

    assertTrue(Files.exists(index));
    assertTrue(Files.exists(blogPost1));

    var indexString = Files.readString(index);
    assertTrue(indexString.contains("<h1>index file for int tests</h1>"));
    assertTrue(indexString.contains("default template"));
    assertTrue(indexString.contains("page path=\"/index\""));
    assertTrue(indexString.contains("node path=\"/\""));
    assertTrue(indexString.contains("url=\"some/path\""));
    assertTrue(indexString.contains("!!Im jsHelper!!"));
    assertTrue(indexString.contains("removeProtocol=\"some.web/site\""));
    assertTrue(indexString.contains("removeString=\"string to remove\""));

    var blogPost1String = Files.readString(blogPost1);
    assertTrue(blogPost1String.contains("<h2>post1</h2>"));
    assertTrue(blogPost1String.contains("url=\"../some/path\""));
    assertTrue(blogPost1String.contains("pagePath=\"/blog/post1\""));
    assertTrue(blogPost1String.contains("pageParentPath=\"/blog\""));
    assertTrue(blogPost1String.contains("blog template"));
    assertTrue(blogPost1String.contains("somekey=\"some value on node\""));
    assertTrue(blogPost1String.contains("rootkey=\"some value on root node\""));

    var blogPost2String = Files.readString(blogPost2);
    assertTrue(blogPost2String.contains("<h1>md title</h1>"));
    assertTrue(blogPost2String.contains("this was a production run"));

    // test yaml page data load
    var sampleYamlPageString = Files.readString(sampleYamlPage);
    assertTrue(sampleYamlPageString.contains("<h1>the sample yaml page</h1>"));

    // test sass compilation
    Path mainCss = outputPath.resolve("style/main.css");
    Path mainCssMap = outputPath.resolve("style/main.css.map");
    assertTrue(Files.exists(mainCss));
    assertTrue(Files.exists(mainCssMap));

    var styleString = Files.readString(mainCss);
    assertTrue(styleString.contains("background: red;"));

    // assets copier tests
    assertTrue(Files.exists(outputPath.resolve("assets/a.txt")));
    assertTrue(Files.exists(outputPath.resolve("images/b.png")));
    assertTrue(Files.exists(outputPath.resolve("images/some/sub/dir/c.png")));
    assertTrue(indexString.contains("cPngAdd=\"build/tmp/int-test/images/some/sub/dir/c.png\""));

    String aTxtString = Files.readString(outputPath.resolve("assets/a.txt"));
    assertEquals("asset file a\n", aTxtString);

    long bPngSize = Files.size(outputPath.resolve("images/b.png"));
    assertEquals(3979, bPngSize);

    // storyblok pages
    assertTrue(Files.exists(outputPath.resolve("storyblok/index.html")));
    assertTrue(Files.exists(outputPath.resolve("storyblok/home/index.html")));
    assertTrue(Files.exists(outputPath.resolve("storyblok/fa/index.html")));

    String homeStory = Files.readString(outputPath.resolve("storyblok/home/index.html"));
    assertTrue(homeStory.contains("headline: Hello world!dsfasdf"));
    assertTrue(homeStory.contains("columns.0.name: Feature 1"));
    assertTrue(homeStory.contains("<!--#storyblok#{\"name\": \"grid\", \"space\": \"89168\", \"uid\": \"54be8add-9deb-4196-be78-e62cdfbc7f57\", "));

    assertTrue(Files.exists(outputPath.resolve("dump-page.html")));
    String dumpPage = Files.readString(outputPath.resolve("dump-page.html"));
    assertTrue(dumpPage.contains("Page dump: DataPage("));
    assertTrue(dumpPage.contains("Node dump: DataNode("));

    // TODO: 29.01.20 add tests for OpenAPI plugin!
    // TODO: 29.01.20 add test for HandlebarsCompiler's start-node config
  }

}
