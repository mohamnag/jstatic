/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic;

import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.data_tree.DataPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BuildContextTest {

  private BuildContext buildContext;

  @BeforeEach
  void setUp() {
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
  }

  @Test
  void shallReturnCorrectFullName() {

    DataPage page =
      buildContext
        .getDataTree()
        .getOrCreatePageWithPath(
          "testPage",
          List.of("nodeLevel1", "nodeLevel2")
        );

    DataNode nodeLevel1 =
      buildContext
        .getDataTree()
        .getOrCreateNodeWithPath(
          List.of("nodeLevel1")
        );

    DataNode nodeLevel2 =
      buildContext
        .getDataTree()
        .getOrCreateNodeWithPath(
          List.of("nodeLevel1", "nodeLevel2")
        );

    DataNode rootNode =
      buildContext
        .getDataTree()
        .getRootNode();

    assertEquals("/nodeLevel1/nodeLevel2/testPage", page.getPath());
    assertEquals("/nodeLevel1", nodeLevel1.getPath());
    assertEquals("/nodeLevel1/nodeLevel2", nodeLevel2.getPath());
    assertEquals("/", rootNode.getPath());
  }

  @Test
  void shallNotRecreateNodesOrPages() {

    buildContext
      .getDataTree()
      .getOrCreatePageWithPath("testPage", List.of("nodeLevel1", "nodeLevel2"));

    buildContext
      .getDataTree()
      .getOrCreatePageWithPath("testPage", List.of("nodeLevel1", "nodeLevel2"));

    assertEquals(
      1,
      buildContext.getDataTree().getRootNode().getSubNodes().size()
    );


    assertEquals(
      0,
      buildContext.getDataTree().getRootNode().getPages().size()
    );

    assertEquals(
      1,
      buildContext.getDataTree().getRootNode().getSubNode("nodeLevel1").get().getSubNodes().size()
    );

    assertEquals(
      0,
      buildContext.getDataTree().getRootNode().getSubNode("nodeLevel1").get().getPages().size()
    );

    assertEquals(
      0,
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("nodeLevel1").get()
        .getSubNode("nodeLevel2").get()
        .getSubNodes()
        .size()
    );

    assertEquals(
      1,
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("nodeLevel1").get()
        .getSubNode("nodeLevel2").get()
        .getPages()
        .size()
    );
  }

  @Test
  void shallReturnCorrectRootNodeFullName() {

    var indexPage =
      buildContext
        .getDataTree()
        .getOrCreatePageWithPath("index", List.of());

    assertEquals(
      "/index",
      indexPage.getPath()
    );
  }

  @Test
  void shallResolveDataFromParentNodes() {

    DataPage page =
      buildContext
        .getDataTree()
        .getOrCreatePageWithPath(
          "page",
          List.of("root", "node")
        );

    DataNode rootNode =
      buildContext
        .getDataTree()
        .getOrCreateNodeWithPath(List.of("root"));

    rootNode.addData("datakey", "datavalue");

    Optional<Object> value = page.getRecursiveData("datakey");

    assertTrue(value.isPresent());
    assertTrue(value.get() instanceof String);
    assertEquals("datavalue", value.get());
  }

}
