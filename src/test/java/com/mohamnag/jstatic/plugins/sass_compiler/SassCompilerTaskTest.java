/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.sass_compiler;

import com.mohamnag.jstatic.BuildContext;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SassCompilerTaskTest {

  @Test
  void shallCompileScssFiles() throws Exception {

    var buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
    var sassCompilerTask = new SassCompilerTask(buildContext.getConfig());

    Path outputPath = Path.of("build/tmp/testSassCompiler");
    if (outputPath.toFile().exists()) {
      Files
        .walk(outputPath)
        .sorted(Comparator.reverseOrder())
        .map(Path::toFile)
        .forEach(File::delete);
    }

    sassCompilerTask.run(buildContext);

    assertTrue(outputPath.resolve("main.css").toFile().exists());
    assertTrue(outputPath.resolve("main.css.map").toFile().exists());

    var css = Files.readString(outputPath.resolve("main.css"));
    assertTrue(css.contains("background: blue"));
  }
}
