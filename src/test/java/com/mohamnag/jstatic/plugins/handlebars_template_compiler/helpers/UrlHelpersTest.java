/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler.helpers;

import com.github.jknack.handlebars.Handlebars;
import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.data_tree.DataPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UrlHelpersTest {

  private Handlebars handlebars;
  private DataPage l3Page;
  private BuildContext buildContext;

  @BeforeEach
  void setUp() {
    handlebars = new Handlebars();
    handlebars.registerHelpers(new UrlHelpers());

    // analog to how its done in com.mohamnag.jstatic.plugins.handlebars_template_compiler.HandlebarsTemplateCompilerTask#compilePage
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");

    l3Page =
      buildContext
        .getDataTree()
        .getOrCreatePageWithPath(
          "testPage",
          List.of(
            "path",
            "to",
            "node"
          ));

  }

  @Test
  void relativeUrlHelper_shall_BeAbleToHandleDifferentInputs() throws Exception {

    assertEquals(
      "../../../absolute/url",
      handlebars
        .compileInline("{{ relativeUrl '/absolute/url' }}")
        .apply(Map.of(
          "page", l3Page,
          "buildContext", buildContext
        ))
    );

    assertEquals(
      "",
      handlebars
        .compileInline("{{ relativeUrl }}")
        .apply(Map.of(
          "page", l3Page,
          "buildContext", buildContext
        ))
    );

    assertEquals(
      "",
      handlebars
        .compileInline("{{ relativeUrl null }}")
        .apply(Map.of(
          "page", l3Page,
          "buildContext", buildContext
        ))
    );

    assertEquals(
      "",
      handlebars
        .compileInline("{{ relativeUrl 0 }}")
        .apply(Map.of(
          "page", l3Page,
          "buildContext", buildContext
        ))
    );

  }

}
