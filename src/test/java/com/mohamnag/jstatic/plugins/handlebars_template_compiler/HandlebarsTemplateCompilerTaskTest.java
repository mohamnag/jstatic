/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler;

import com.mohamnag.jstatic.BuildContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HandlebarsTemplateCompilerTaskTest {

  private BuildContext buildContext;
  private HandlebarsTemplateCompilerTask compilerTask;

  @BeforeEach
  void setUp() {
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
    compilerTask = new HandlebarsTemplateCompilerTask(buildContext.getConfig());
  }

  @Test
  void shallLoadDefaultTemplate() {

    buildContext
      .getDataTree()
      .getOrCreatePageWithPath("testPage", List.of())
      .addData("testData", "SomeDataWeExpectToBeIncludedInCompiledPage");

    compilerTask
      .run(buildContext);

    buildContext
      .getDataTree()
      .getRootNode()
      .getPages()
      .values()
      .forEach(dataPage -> {
        assertNotNull(dataPage.getData("compiledHandlebars"));
        assertFalse(dataPage.getData("compiledHandlebars").isEmpty());
        assertTrue(dataPage.getData("compiledHandlebars").toString().contains("SomeDataWeExpectToBeIncludedInCompiledPage"));
        assertTrue(dataPage.getData("compiledHandlebars").toString().contains("!!Im jsHelper!!"));
      });
  }

}
