/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.ComponentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OpenApiProcessorConfigTest {

  private BuildContext buildContext;

  @BeforeEach
  void setUp() {
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
  }

  @Test
  void shall_loadConfig() {
    var config = OpenApiProcessorConfig.extract(buildContext.getConfig());

    assertEquals(config.getPackageName(ComponentType.SCHEMAS), "com.mohamnag.test.schemas");
    assertEquals(config.getTemplate(ComponentType.SCHEMAS), "tests/openapi/schema");
  }
}
