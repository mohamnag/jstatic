package com.mohamnag.jstatic.plugins.openapi_processor;

import com.mohamnag.jstatic.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
public class OpenApiIntegrationTest {

  private final Path outputPath = Paths.get("build/tmp/open-api-tests/");

  @Test
  void fullRun() throws Exception {

    // given output dir is cleaned up and empty
    if (outputPath.toFile().exists()) {
      try (var pathStream = Files.walk(outputPath)) {
        var allDeleted = pathStream
          .sorted(Comparator.reverseOrder())
          .map(Path::toFile)
          .map(File::delete)
          .reduce(true, (aBoolean, aBoolean2) -> aBoolean && aBoolean2);

        if (!allDeleted) {
          log.warn("Not all output files under {} could be deleted. Might see interference from old runs.", outputPath);
        }
      }
    }

    // when running the application
    Application.main(new String[]{"--config=src/test/resources/open-api-tests/config.yaml"});

    // then expect that resulting data is found on output
    Path index = outputPath.resolve("com/example/openapi/api_one/schemas/ErrorResponse.java");
    assertTrue(Files.exists(index));

  }

}
