/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.html_file_dumper;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.data_tree.DataPage;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FileDumperTaskTest {

  @Test
  void shallDumpDataFromContext() throws IOException {
    BuildContext context = new BuildContext("src/test/resources/unit-test/config.yaml");

    DataPage page =
      context
        .getDataTree()
        .getOrCreatePageWithPath(
          "testpage",
          List.of("directory", "in", "output")
        );

    page.addData("compiled", "<p>some text that should be in file</p>");

    FileDumperTask dumperTask = new FileDumperTask(context.getConfig());
    dumperTask.run(context);

    File outputFile = new File("build/tmp/testFileDumper/directory/in/output/testpage.html");
    assertTrue(outputFile.exists());

    String readFile = Files.readString(outputFile.toPath());
    assertEquals(
      "<p>some text that should be in file</p>",
      readFile
    );
  }

}
