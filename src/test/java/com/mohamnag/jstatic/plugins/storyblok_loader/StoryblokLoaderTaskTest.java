/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.storyblok_loader;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.mohamnag.jstatic.BuildContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class StoryblokLoaderTaskTest {

  @RegisterExtension
  static WireMockExtension storyblokMock = WireMockExtension.newInstance()
    .options(WireMockConfiguration.options().port(8090))
    .build();

  private BuildContext buildContext;
  private StoryblokLoaderTask task;

  @BeforeEach
  void setUp() {
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
    task = new StoryblokLoaderTask(buildContext.getConfig());
  }

  @Test
  void shallLoadDataIntoTree() {
    storyblokMock
      .stubFor(get(urlPathEqualTo("/stories"))
        .withQueryParam("token", equalTo("blablatoken"))
        .withQueryParam("version", equalTo("draft"))
        .withQueryParam("cv", matching("[0-9]+"))
        .willReturn(
          ok()
            .withBodyFile("storyblok.json")
        )
      );

    task.run(buildContext);

    var rootPages = buildContext
      .getDataTree()
      .getRootNode()
      .getPages();

    assertEquals(rootPages.size(), 1);
    assertNotNull(rootPages.get("index").getData().get("story"));

    var homeNodePages = buildContext
      .getDataTree()
      .getOrCreateNodeWithPath(List.of("home"))
      .getPages();

    assertEquals(homeNodePages.size(), 1);
    assertNotNull(homeNodePages.get("index").getData().get("story"));

    var faNodePages = buildContext
      .getDataTree()
      .getOrCreateNodeWithPath(List.of("fa"))
      .getPages();

    assertEquals(faNodePages.size(), 1);
    assertNotNull(faNodePages.get("index").getData().get("story"));

    storyblokMock
      .verify(1, getRequestedFor(urlPathEqualTo("/stories")));
  }
}
