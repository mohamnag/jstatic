/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.front_matter_loader;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.data_tree.DataPage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MarkdownFrontMatterLoaderTaskTest {

  private BuildContext buildContext;

  @BeforeEach
  void setUp() {
    buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");
    MarkdownFrontMatterLoaderTask task = new MarkdownFrontMatterLoaderTask(buildContext.getConfig());
    task.run(buildContext);
  }

  @Test
  public void shallLoadOnlyFiles() {

    assertEquals(
      1,
      buildContext
        .getDataTree()
        .getRootNode()
        .getPages()
        .size()
    );

    assertEquals(
      1,
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("some")
        .get()
        .getPages()
        .size()
    );

    assertEquals(
      1,
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("some")
        .get()
        .getSubNode("sub")
        .get()
        .getSubNode("directory")
        .get()
        .getPages()
        .size()
    );

    assertEquals(
      0,
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("some")
        .get()
        .getSubNode("sub")
        .get()
        .getPages()
        .size()
    );
  }

  @Test
  void shallStripExtensionFromFileName() {
    assertNotNull(
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("some")
        .get()
        .getSubNode("sub")
        .get()
        .getSubNode("directory")
        .get()
        .getPage("test_page_down")
    );
  }

  @Test
  void shallExtractDataFromFile() {
    DataPage dataPage =
      buildContext
        .getDataTree()
        .getRootNode()
        .getSubNode("some")
        .get()
        .getSubNode("sub")
        .get()
        .getSubNode("directory")
        .get()
        .getPage("test_page_down")
        .get();

    assertEquals(
      "value1",
      dataPage.getData("field1").get()
    );

    assertEquals(
      "\n# Try Markdown\n",
      dataPage.getData("body").get()
    );
  }

}
