/*
 *  Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.assets_copier;

import com.mohamnag.jstatic.BuildContext;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AssetsCopierTaskTest {

  @Test
  void shallCopyFilesOver() throws Exception {

    var buildContext = new BuildContext("src/test/resources/unit-test/config.yaml");

    var assetsCopierTask = new AssetsCopierTask(buildContext.getConfig());
    assetsCopierTask.run(buildContext);

    Path outputPath = Path.of("build/tmp/testAssetsCopier");

    assertTrue(Files.exists(outputPath.resolve("a.txt")));
    assertTrue(Files.exists(outputPath.resolve("sub/b.txt")));

    String aTxtString = Files.readString(outputPath.resolve("a.txt"));
    assertEquals(aTxtString, "test file a\n");

    String bTxtString = Files.readString(outputPath.resolve("sub/b.txt"));
    assertEquals(bTxtString, "test file b\n");
  }
}
