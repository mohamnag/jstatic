Handlebars.registerHelper(
  'concat',
  function (text, prefix) {
    return Array.prototype.slice.call(arguments, 0, -1).join('');
  })

Handlebars.registerHelper(
  'prependLines',
  function (text, prefix) {
    var lines = text.split('\n');
    for (var i = 0; i < lines.length; i++) {
      lines[i] = prefix + lines[i];
    }
    return lines.join('\n');
  })

function camelCase(text) {
  return text.charAt(0).toUpperCase() + text.slice(1);
}

Handlebars.registerHelper('camelCase', camelCase)

function resolveRef(component, ref) {
  if (!ref) {
    return 'null ref'
  }

  let context = component.getContext();
  if (!context) {
    return context == null;
  }

  return context.resolveReference(ref);
}

Handlebars.registerHelper('resolveRef', resolveRef)

function extractRefName(ref) {
  let refString = ref.toString();
  return refString.substring(refString.lastIndexOf('/') + 1)
}

Handlebars.registerHelper('extractRefName', extractRefName)

Handlebars.registerHelper(
  'fieldType',
  function fieldType(component, key) {

    if (component.ref) {
      let refComponent = resolveRef(component, component.ref);
      let refName = extractRefName(component.ref);

      return fieldType(refComponent, refName);
    }

    if (component.type == 'string') {
      if (component.enumValues) {
        return camelCase(key);

      } else if (component.format == 'date-time') {
        return 'Instant'

      } else {
        return 'String'
      }
    }

    if (component.type == 'boolean') {
      return 'Boolean';
    }

    if (component.type == 'integer') {
      return 'Integer';
    }

    if (component.type == 'number') {
      return 'BigDecimal';
    }

    if (component.type == 'object') {
      return camelCase(key);
    }

    if (component.type == 'array') {
      return 'List<' + fieldType(component.items, key) + '>';
    }

    return 'unsupported type: ' + component.type;
  })

Handlebars.registerHelper(
  'getterName', function (component, key) {
    if (component.type == 'boolean') {
      return 'is' + camelCase(key);
    }

    return 'get' + camelCase(key);
  });

Handlebars.registerHelper(
  'setterName', function (component, key) {
    if (component.type == 'boolean') {
      return 'set' + camelCase(key);
    }

    return 'set' + camelCase(key);
  });

Handlebars.registerHelper(
  'eq', function (v1, v2) {
    return v1 == v2
  });

Handlebars.registerHelper(
  'ne', function (v1, v2) {
    return v1 != v2;
  });

Handlebars.registerHelper(
  'lt', function (v1, v2) {
    return v1 < v2;
  });

Handlebars.registerHelper(
  'gt', function (v1, v2) {
    return v1 > v2;
  });

Handlebars.registerHelper(
  'lte', function (v1, v2) {
    return v1 <= v2;
  });

Handlebars.registerHelper(
  'gte', function (v1, v2) {
    return v1 >= v2;
  });

Handlebars.registerHelper(
  'not', function (v1) {
    return !v1;
  });

Handlebars.registerHelper(
  'and', function () {
    return Array.prototype.slice.call(arguments, 0, -1).every(Boolean);
  });

Handlebars.registerHelper(
  'or', function () {
    return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
  });
