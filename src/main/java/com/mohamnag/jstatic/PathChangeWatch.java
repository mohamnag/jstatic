/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Slf4j
public class PathChangeWatch {

  private final WatchService watchService;
  private final Consumer<List<WatchEvent<?>>> operation;

  public PathChangeWatch(
    WatchService watchService,
    List<Path> underWatchPaths,
    Consumer<List<WatchEvent<?>>> operation
  ) {
    this.watchService = watchService;
    this.operation = operation;

    underWatchPaths
      .forEach(path -> {
        if (Files.isDirectory(path)) {
          this.addDirHierarchyToWatch(path);

        } else {
          this.addSingleDirToWatch(path.toAbsolutePath().getParent());
        }
      });
  }

  private void addDirHierarchyToWatch(Path path) {
    try (
      var dirs = Files
        .walk(path)
        .filter(Files::isDirectory)
    ) {
      dirs.forEach(this::addSingleDirToWatch);

    } catch (IOException e) {
      log.warn("could not watch directory {}", path, e);
    }
  }

  private void addSingleDirToWatch(Path path) {
    try {
      path.register(
        watchService,
        StandardWatchEventKinds.ENTRY_CREATE,
        StandardWatchEventKinds.ENTRY_DELETE,
        StandardWatchEventKinds.ENTRY_MODIFY
      );

    } catch (IOException e) {
      log.warn("could not watch file {}", path, e);
    }
  }

  public void poll(int time, TimeUnit unit) throws InterruptedException {
    WatchKey key = watchService.poll(time, unit);

    if (key != null) {
      for (WatchEvent<?> event : key.pollEvents()) {
        log.debug("change {} detected in file '{}'", event.kind(), event.context());
      }

      operation.accept(key.pollEvents());
      key.reset();
    }
  }

}
