package com.mohamnag.jstatic.data_tree;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Getter
public class DataTree {

  private DataNode rootNode = new DataNode();

  /**
   * Find or create (recursively) nodes for given path and return it.
   */
  public DataNode getOrCreateNodeWithPath(List<String> path) {
    DataNode lastNode = rootNode;
    for (String pathElement : path) {
      lastNode = lastNode.findOrCreateChildNode(pathElement);
    }
    return lastNode;
  }

  /**
   * Creates a page at a node with given path. If necessary all the nodes on the
   * path will be created too.
   */
  public DataPage getOrCreatePageWithPath(String pageName, List<String> path) {
    return this
      .getOrCreateNodeWithPath(path)
      .findOrCreatePage(pageName);
  }

  public Optional<DataPage> getPageWithPath(List<String> path) {
    if (path.isEmpty()) {
      return Optional.empty();
    }

    var fullPath = new ArrayList<>(path);
    var pageName = fullPath.removeLast();
    return getOrCreateNodeWithPath(fullPath).getPage(pageName);
  }

  public void forAllPages(Consumer<DataPage> action) {
    List<DataNode> nodes = new ArrayList<>();
    nodes.add(
      getRootNode()
    );

    while (!nodes.isEmpty()) {
      var node = nodes.remove(0);

      node
        .getPages()
        .values()
        .forEach(action);

      nodes.addAll(
        node.getSubNodes().values()
      );
    }
  }
}
