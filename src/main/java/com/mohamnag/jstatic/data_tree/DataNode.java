package com.mohamnag.jstatic.data_tree;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This represents a node in {@link DataTree}. A node is not
 * compiled and do not represent a dumped page at the end but
 * might carry shared data for its child pages.
 * <p>
 * A node might contain other nodes as children and might have
 * a parent node. A node might contain many {@link DataPage}.
 */
@Getter
@ToString(exclude = {"parent"})
public class DataNode {

  private final String name;
  private final DataNode parent;
  private final int depth;
  private final Map<String, Object> data = new HashMap<>();
  private final Map<String, DataNode> subNodes = new HashMap<>();
  private final Map<String, DataPage> pages = new HashMap<>();

  private DataNode(String name, DataNode parent) {
    this.name = name;
    this.parent = parent;
    this.depth = parent != null ? parent.depth + 1 : 0;
  }

  DataNode() {
    this("", null);
  }

  DataNode findOrCreateChildNode(String subPath) {
    return this.subNodes.computeIfAbsent(
      subPath,
      s -> new DataNode(subPath, this)
    );
  }

  DataPage findOrCreatePage(String name) {
    return this.pages.computeIfAbsent(
      name,
      s -> new DataPage(name, this)
    );
  }

  public Map<String, DataNode> getSubNodes() {
    return subNodes;
  }

  public Map<String, DataPage> getPages() {
    return pages;
  }

  public Optional<DataNode> getSubNode(String nodeName) {
    return Optional.ofNullable(
      subNodes.get(nodeName));
  }

  public Optional<DataPage> getPage(String pageName) {
    return Optional.ofNullable(
      pages.get(pageName));
  }

  public void addAllData(Map<String, Object> data) {
    if(data != null) {
      this.data.putAll(data);
    }
  }

  public void addData(String key, Object value) {
    this.data.put(key, value);
  }

  /**
   * @return absolute path of this node
   */
  public String getPath() {
    return "/" + String.join("/", getPathElements());
  }

  public List<String> getPathElements() {
    List<String> list;
    if (parent == null) {
      list = new ArrayList<>();

    } else {
      list = new ArrayList<>(parent.getPathElements());
      list.add(this.name);
    }

    return list;
  }

  /**
   * Get value for given key first looking on own data and then recursively in parent nodes
   * upwards.
   */
  public Optional<Object> getRecursiveData(String key) {
    if (this.data.containsKey(key)) {
      return Optional.of(this.data.get(key));

    } else if (this.parent != null) {
      return parent.getRecursiveData(key);

    } else {
      return Optional.empty();
    }
  }

  public <T> Optional<T> getData(String key) {
    return Optional.ofNullable((T) this.data.get(key));
  }

  public <T> T getData(String key, T initValue) {
    this.data.putIfAbsent(key, initValue);
    return (T) this.data.get(key);
  }

}
