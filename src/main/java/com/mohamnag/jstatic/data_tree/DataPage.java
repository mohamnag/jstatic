package com.mohamnag.jstatic.data_tree;

import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * This represents a page in {@link DataTree}. A page contains own
 * data and will get compiled down to a final file.
 */
@Getter
@ToString(exclude = {"parent"})
public class DataPage {

  /**
   * Path of this page relative to the container node
   */
  private final String name;
  private final DataNode parent;
  private final Map<String, Object> data = new HashMap<>();

  DataPage(String name, DataNode parent) {
    this.name = name;
    this.parent = parent;
  }

  public Map<String, Object> getData() {
    return Collections.unmodifiableMap(data);
  }

  public Optional<Object> getData(String key) {
    return Optional.ofNullable(data.get(key));
  }

  /**
   * Get value for given key first looking on own data and then recursively in parent nodes
   * upwards.
   */
  public Optional<Object> getRecursiveData(String key) {
    if (this.data.containsKey(key)) {
      return Optional.of(this.data.get(key));

    } else {
      return parent.getRecursiveData(key);
    }
  }

  public void addData(String key, Object value) {
    this.data.put(key, value);
  }

  /**
   * @return absolute path of this page
   */
  public String getPath() {
    String parentPath = parent.getPath();
    return (parentPath.endsWith("/") ? parentPath : parentPath.concat("/")).concat(this.name);
  }

  public List<String> getPathElements() {
    List<String> list = new ArrayList<>(parent.getPathElements());
    list.add(this.name);
    return list;
  }

  /**
   * Will resolve a given path which is relative to this page into an absolute one that can be resolved from the
   * {@link DataTree} for target page.
   */
  public List<String> resolveRelativePath(List<String> relativePath) {
    var targetPath = new ArrayList<>(getPathElements());
    // dropping page's own
    targetPath.removeLast();

    for (String pathPiece : relativePath) {
      // dot can be ignored
      if (".".equals(pathPiece)) {
        continue;
      }
      // traverse one step up
      else if ("..".equals(pathPiece)) {
        targetPath.removeLast();
        continue;
      }

      // add path
      targetPath.addLast(pathPiece);
    }

    return Collections.unmodifiableList(targetPath);
  }

  public void addAllData(Map<String, Object> data) {
    this.data.putAll(data);
  }
}
