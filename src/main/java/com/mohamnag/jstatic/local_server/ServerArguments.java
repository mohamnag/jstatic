/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.local_server;

import java.nio.file.Path;

/**
 * Accepts an argument starting with {@value CLI_ARG_PREFIX_SERVE} followed by the path of directory that should be
 * served and optionally the port to serve content over.
 * <p>
 * For example (please ignore the quotes):
 * <code>{@value CLI_ARG_PREFIX_SERVE}=path/to/dir</code>
 * or
 * <code>{@value CLI_ARG_PREFIX_SERVE}=path/to/dir:8082</code>
 * <p>
 * The port will default to {@value DEFAULT_PORT} if not provided.
 * <p>
 * Optionally a third parameter can be set to constant {@value ENABLE_SSL} to enable SSL with a self-signed certificate
 * on chosen port. For example:
 * <code>{@value CLI_ARG_PREFIX_SERVE}=path/to/dir::ssl</code>
 * or
 * <code>{@value CLI_ARG_PREFIX_SERVE}=path/to/dir:8082:ssl</code>
 */
public record ServerArguments(Path servingDirectory, int port, boolean enableSsl) {
  public static final String CLI_ARG_PREFIX_SERVE = "--serve";

  private static final int DEFAULT_PORT = 8080;
  private static final String ENABLE_SSL = "ssl";

  public static boolean canParse(String s) {
    return s.startsWith(CLI_ARG_PREFIX_SERVE + "=");
  }

  public static ServerArguments parse(String argumentString) {
    var serverParams = argumentString.split("[=:]");

    if (serverParams.length < 1) {
      throw new IllegalArgumentException("To use the server mode, you have to at least provide the directory to be served. The format is '--serve=/path/to/dir:8080'");
    }

    var servingDirectory = Path.of(serverParams[1]).toAbsolutePath();
    var dir = servingDirectory.toFile();
    if (!dir.exists() || !dir.isDirectory()) {
      throw new IllegalArgumentException("Provided path %s either does not exist or is not a directory".formatted(dir));
    }

    var port = serverParams.length > 2 && !serverParams[2].isBlank() ? Integer.parseInt(serverParams[2]) : DEFAULT_PORT;

    var enableSsl = serverParams.length > 3 && ENABLE_SSL.equalsIgnoreCase(serverParams[3]);

    return new ServerArguments(servingDirectory, port, enableSsl);
  }
}
