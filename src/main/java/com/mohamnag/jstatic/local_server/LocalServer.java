package com.mohamnag.jstatic.local_server;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpHandlers;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsServer;
import com.sun.net.httpserver.SimpleFileServer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * Starts a local server that does two things:
 * <ul>
 *   <li>serves contents of a requested directory over requested port</li>
 *   <li>listens to any request with PUT method (no matter which path) and triggers a rebuild</li>
 * </ul>
 * The server can be optionally started with SSL using a self-signed certificate. This server (with or without SSL) is
 * intended for development purposes only and should not be made publicly available.
 */
@Slf4j
public class LocalServer {

  private static final String KEYSTORE_PASSWORD = "password";
  private static final InputStream KEYSTORE_FILE = LocalServer.class.getResourceAsStream("/keystore.jks");

  private final Path servingDirectory;
  private final int port;
  private final Runnable onRebuildRequested;
  private final HttpHandler deferringUpdateHttpHandler = HttpHandlers.of(204, Headers.of(), "");
  private final boolean enableSsl;

  public LocalServer(ServerArguments arguments, Runnable onRebuildRequested) {
    this.onRebuildRequested = onRebuildRequested;
    this.port = arguments.port();
    this.servingDirectory = arguments.servingDirectory();
    this.enableSsl = arguments.enableSsl();
  }

  public void start() {
    var socketAddress = new InetSocketAddress(port);
    var requestHandler = HttpHandlers.handleOrElse(
        r -> r.getRequestMethod().equals("PUT"),
        exchange -> {
          log.info("HTTP rebuild triggered");
          onRebuildRequested.run();
          deferringUpdateHttpHandler.handle(exchange);
        },
        SimpleFileServer.createFileHandler(servingDirectory)
      );

    var startLoggerFilter = Filter.beforeHandler(
        "Logs exchanges",
      e -> {
        if (log.isInfoEnabled()) {
          var rayId = UUID.randomUUID();
          e.setAttribute("ray-id", rayId);
          log.info(
            "Start request [{}] {} - \"{} {} {}\"",
            rayId,
            e.getRemoteAddress().getHostString(),
            e.getRequestMethod(),
            e.getRequestURI(),
            e.getProtocol()
          );
        }
      });

    var endLoggerFilter = Filter.afterHandler(
      "Logs exchanges",
      e -> {
        if (log.isInfoEnabled()) {
          var rayId = e.getAttribute("ray-id");
          log.info(
            "End request [{}] status {}",
            rayId,
            e.getResponseCode()
          );
        }
      });

    try {
      if (enableSsl) {
        startHttpsServer(socketAddress, requestHandler, startLoggerFilter, endLoggerFilter);

      } else {
        startHttpServer(socketAddress, requestHandler, startLoggerFilter, endLoggerFilter);
      }
    } catch (Exception e) {
      log.warn("Failed starting content server on port {}", port, e);
    }
  }

  @SneakyThrows
  private void startHttpsServer(
    InetSocketAddress socketAddress,
    HttpHandler requestHandler,
    Filter... filters
  ) {

    var httpsConfigurator = new HttpsConfigurator(buildSslContext());
    log.info("Initialized SSL context");

    var server = HttpsServer.create(socketAddress, 10, "/", requestHandler, filters);
    server.setHttpsConfigurator(httpsConfigurator);
    server.start();
    log.info("HTTPS Server started successfully serving {} to {}", servingDirectory, socketAddress);
  }

  @SneakyThrows
  private static SSLContext buildSslContext() {
    KeyStore store = KeyStore.getInstance("JKS");
    store.load(KEYSTORE_FILE, KEYSTORE_PASSWORD.toCharArray());

    KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
    keyFactory.init(store, KEYSTORE_PASSWORD.toCharArray());

    TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    trustFactory.init(store);

    SSLContext sslContext = SSLContext.getInstance("TLS");
    sslContext.init(keyFactory.getKeyManagers(), trustFactory.getTrustManagers(), new SecureRandom());
    return sslContext;
  }

  private void startHttpServer(
    InetSocketAddress socketAddress,
    HttpHandler handler,
    Filter... filters
  ) throws IOException {

    HttpServer
      .create(socketAddress, 10, "/", handler, filters)
      .start();

    log.info("HTTP Server started successfully serving {} to {}", servingDirectory, socketAddress);
  }
}
