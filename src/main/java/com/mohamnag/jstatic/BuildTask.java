/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import java.nio.file.Path;
import java.util.List;

/**
 * All build phase plugins shall implement this interface.
 * <p>
 * Each plugin might have either a no-args constructor or one with a single argument of type
 * {@link com.mohamnag.jstatic.config.Config}. If both, the one with configuration is preferred.
 * <p>
 * If a plugin depends on configuration, it should received {@link com.mohamnag.jstatic.config.Config}
 * as constructor argument and use it for configuration during the construction process before
 * any other method is called from outside.
 */
public interface BuildTask {

  // TODO replace BuildContext with DataTree
  void run(BuildContext buildContext);

  /**
   * If a plugin depends on file input, it shall return list of its input paths to be watched
   * for changes. Paths can be either files or directories. In case of directories, only root
   * path are necessary to be listed and if input path has subdirectories, they will be
   * automatically watched.
   */
  List<Path> underWatchInputPaths();

  /**
   * If a plugin depends on config files in addition to the general config file of the application,
   * like functionality extensions stored in files, it shall return these paths here. A change
   * detected in these paths will result in recreation of the plugin.
   */
  List<Path> underWatchConfigPaths();

}
