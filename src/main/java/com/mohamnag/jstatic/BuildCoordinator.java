/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import com.mohamnag.jstatic.plugin_loader.PluginLoader;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class BuildCoordinator {

  private final BuildContext context;
  private final PluginLoader pluginLoader;
  private final List<Path> watchedConfigPaths;

  public BuildCoordinator(String configFilePath) {
    this.context = new BuildContext(configFilePath);
    this.pluginLoader = new PluginLoader(this.context.getConfig());
    this.watchedConfigPaths = new ArrayList<>(pluginLoader.getUnderWatchConfigPaths());
    this.watchedConfigPaths.add(Path.of(configFilePath));
  }

  public void run() {
    context.resetPhase();
    do {
      runPhase(context.getCurrentPhase());
    } while (context.goToNextPhase());
  }

  private void runPhase(Phase currentPhase) {
    log.info("starting phase {}", currentPhase);

    pluginLoader
      .getLoadedPlugins()
      .get(currentPhase)
      .forEach(buildTask -> {
        try {
          buildTask.run(context);

        } catch (Exception e) {
          log.warn(
            "task {} failed execution",
            buildTask.getClass().getName(),
            e
          );
        }
      });

    log.info("finished phase {}", currentPhase);
  }

  public List<Path> getUnderWatchInputPaths() {
    return pluginLoader.getUnderWatchInputPaths();
  }

  public List<Path> getUnderWatchConfigPaths() {
    return watchedConfigPaths;
  }

}
