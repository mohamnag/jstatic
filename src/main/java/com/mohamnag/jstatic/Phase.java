/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

public enum Phase {
  LOAD_TEMPLATES,
  LOAD_DATA_TREE,
  PROCESS_ASSETS,
  COMPILE_TEMPLATES,
  DUMP_TREE,
  ;
}
