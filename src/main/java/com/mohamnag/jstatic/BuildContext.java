/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.config.ConfigLoader;
import com.mohamnag.jstatic.data_tree.DataTree;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class BuildContext {

  private final Config config;
  private final Phase[] phases;
  private Phase currentPhase;
  private DataTree dataTree;

  public BuildContext(String configFilePath) {
    resetPhase();
    this.config = ConfigLoader.load(configFilePath);
    this.dataTree = new DataTree();
    this.phases = Phase.values();
  }

  public void resetPhase() {
    this.currentPhase = Phase.LOAD_TEMPLATES;
  }

  public boolean goToNextPhase() {
    if (hasNextPhase()) {
      this.currentPhase = this.phases[this.currentPhase.ordinal() + 1];
      return true;
    }

    return false;
  }

  public boolean hasNextPhase() {
    return this.currentPhase.ordinal() < this.phases.length - 1;
  }

}
