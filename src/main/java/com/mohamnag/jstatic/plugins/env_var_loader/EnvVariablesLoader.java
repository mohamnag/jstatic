/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.env_var_loader;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.data_tree.DataTree;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Loads data from environment variables into {@link DataTree}'s root {@link DataNode#getData()}.
 * <p>
 * {@value #ENV_VAR_PREFIX}{field name} will be added to root node's data. To use multi-word field names,
 * separate words with a "_".
 * <p>
 * For field names only following chars are allowed: A-Z and 0-9
 * <p>
 * Following values are handled specially (case sensitive):
 * <ul>
 *   <li>"true" into boolean true</li>
 *   <li>"false" into boolean false</li>
 * </ul>
 * <p>
 * Example of conversion between environment variables and roo node fields:
 * <ul>
 *  <li>
 *    <code>{@value #ENV_VAR_PREFIX}ENV=development</code> will be set as data with key <code>env</code> and
 *    value <code>development</code> on {@link DataTree#getRootNode()}'s {@link DataNode#getData()}.
 *  </li>
 *
 *  <li>
 *    <code>{@value #ENV_VAR_PREFIX}SOME_FIELD="the value"</code> will be set as data with key
 *    <code>someField</code> and value <code>the value</code> on {@link DataTree#getRootNode()}'s
 *    {@link DataNode#getData()}.
 *  </li>
 * </ul>
 * <p>
 * This plugin does <b>not</b> support hot reloading in watch mode and in mostly meant to override
 * specific variables in CI and similar environments.
 * <p>
 * The functionality of this plugin is intentionally kept simple and limited to data injection into
 * root node.
 */
public class EnvVariablesLoader implements BuildTask {

  private static final String ENV_VAR_PREFIX = "JSTATIC_DATA_ROOT__";
  private static final Pattern DOT_PATTERN = Pattern.compile("\\.[a-z]");

  @Override
  public void run(BuildContext buildContext) {
    System
      .getenv()
      .entrySet()
      .stream()
      .filter(entry -> entry.getKey().startsWith(ENV_VAR_PREFIX))
      .forEach(entry -> {
        buildContext
          .getDataTree()
          .getRootNode()
          .addData(
            extractFieldNameFromKey(entry.getKey()),
            parseValue(entry.getValue())
          );
      });
  }

  private Object parseValue(String envVarValue) {
    return
      Objects.equals("true", envVarValue) ?
        true :
        Objects.equals("false", envVarValue) ?
          false :
          envVarValue;
  }

  private String extractFieldNameFromKey(String envVarName) {
    String dottedFieldName =
      envVarName
        .substring(ENV_VAR_PREFIX.length())
        .replaceAll("\\.", "")
        .replaceAll("_", ".")
        .toLowerCase()
        .replaceAll("[^a-z0-9.]", "");

    return DOT_PATTERN
      .matcher(dottedFieldName)
      .replaceAll(matchee -> matchee.group().substring(1).toUpperCase());
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return Collections.emptyList();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
