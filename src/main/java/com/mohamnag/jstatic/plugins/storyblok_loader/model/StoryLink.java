package com.mohamnag.jstatic.plugins.storyblok_loader.model;

public record StoryLink(
  String name,
  Long id,
  String uuid,
  String slug,
  String url,
  String full_slug
) {
}
