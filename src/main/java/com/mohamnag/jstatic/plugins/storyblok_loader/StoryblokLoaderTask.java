/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.storyblok_loader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.plugins.storyblok_loader.model.Story;
import com.mohamnag.jstatic.plugins.storyblok_loader.model.StoryListResponse;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This loads the list of stories from Storyblok API, puts full data under the key {@value #KEY_ROOT_DATA_STORYBLOK} in
 * root node. It also creates one page per story in data tree under the node defined by
 * {@link StoryblokLoaderConfig#KEY_TARGET_NODE}.
 * <p/>
 * To determine the path of the page, we use the full slug of the page and will append `/index` to it if it is not
 * already containing it. This is in sync with what Storyblok's UI is expecting from the relation between slug and
 * paths. This means if you want to put a page on root of its directory you either define it as such in the editor or
 * use the slug <code>index</code> for it if not possible.
 * <p/>
 * If you are using links to internal stories, you need to get the <code>id</code> in your link object and lookup the
 * URL from the root level <code>links</code> list of the full data object that is put under root node. If using
 * Hamcrest templates, probably having a helper to do this would be the easiest solution. Something between the lines
 * of:
 * <code>
 *   <pre>
 *    Handlebars.registerHelper(
 *     'resolveStoryblokLinkUrl', function (page, link) {
 *         let storyblokData = page.getRecursiveData('storyblok').orElse(undefined);
 *         if (storyblokData) {
 *             for (const key in storyblokData.links) {
 *                 let linkData = storyblokData.links[key];
 *                 if (linkData.uuid === link.id) {
 *                     return linkData.url;
 *                 }
 *             }
 *         }
 *
 *         return "";
 *     });
 *   </pre>
 * </code>
 * See <a href="https://www.storyblok.com/docs/guide/in-depth/rendering-the-link-field">Storyblok</a> documentation for
 * details on how links work.
 * <p/>
 * See {@link StoryblokLoaderConfig} for configuration.
 */
@Slf4j
public class StoryblokLoaderTask implements BuildTask {
  private static final String BASE_URL = "/stories";
  private static final String KEY_ROOT_DATA_STORYBLOK = "storyblok";

  private final OkHttpClient client = new OkHttpClient();
  private final StoryblokLoaderConfig config;
  private final ObjectMapper objectMapper;

  public StoryblokLoaderTask(Config globalConfig) {
    config = new StoryblokLoaderConfig(globalConfig);
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
  }

  @Override
  public void run(BuildContext buildContext) {
    // TODO: 22.11.23 in watch mode, fetch GET /v2/cdn/spaces/me/ and rebuild if changed
    var url = HttpUrl
      .parse(config.getApiRoot() + BASE_URL)
      .newBuilder()
      .addQueryParameter("token", config.getToken())
      .addQueryParameter("resolve_links", "url")
      .addQueryParameter("version", config.isUseDraftVersion() ? "draft" : "published")
      .addQueryParameter("cv", String.valueOf(Instant.now().getEpochSecond()))
      .build()
      .toString();

    Request request = new Request.Builder()
      .url(url)
      .get()
      .build();

    try (var response = client.newCall(request).execute()) {
      if (!response.isSuccessful()) {
        throw new IOException("Unexpected response code " + response);
      }

      var storyblokData = objectMapper.readValue(response.body().string(), StoryListResponse.class);
      log.debug("Loaded following Storyblok data: {}", storyblokData);

      var dataTree = buildContext.getDataTree();
      dataTree.getRootNode().addData(KEY_ROOT_DATA_STORYBLOK, storyblokData);

      var stories = storyblokData.stories();
      for (Story story : stories) {
        var pagePath = extractPath(config.getTargetNode(), story);

        var page = dataTree
          .getOrCreatePageWithPath(
            pagePath.get(pagePath.size() - 1),
            pagePath.subList(0, pagePath.size() - 1)
          );

        page.addData(config.getDataKey(), story);
        log.debug("Injected story {} to the tree under path {}", story.name(), page.getPath());
      }
    } catch (Exception e) {
      log.warn("Failed loading Storyblok content", e);
    }
  }

  private List<String> extractPath(List<String> rootNodePath, Story story) {
    // prepend all with root node path
    List<String> pagePath = new ArrayList<>(rootNodePath);

    String[] pathFragments;
    // story.path is rather meant for its editor, we rely on its full slug
    var fulledSlug = story.full_slug();
    pathFragments = fulledSlug.split("/");
    pagePath.addAll(Arrays.asList(pathFragments));
    if (!Objects.equals("index", pagePath.getLast())) {
      pagePath.add("index");
    }

    return Collections.unmodifiableList(pagePath);
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return List.of();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return List.of();
  }

}
