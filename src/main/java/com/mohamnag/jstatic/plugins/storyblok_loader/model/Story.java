/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.storyblok_loader.model;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public record Story(
  String name,
  Instant created_at,
  Instant published_at,
  Long id,
  UUID uuid,
  Map<String, Object> content,
  String slug,
  String full_slug,
  Instant sort_by_date,
  Long position,
  List<String> tag_list,
  Boolean is_startpage,
  Long parent_id,
  Map<String, Object> meta_data,
  UUID group_id,
  Instant first_published_at,
  String release_id,
  String lang,
  String path,
  List<String> alternates,
  String default_full_slug,
  String translated_slugs
) {
}
