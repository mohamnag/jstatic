/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.storyblok_loader.model;

import java.util.List;

public record StoryListResponse(List<Story> stories, Long cv, List<Object> rels, List<StoryLink> links) {
}
