/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.storyblok_loader;

import com.mohamnag.jstatic.config.Config;
import lombok.Getter;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Loads Storyblok Plugin's config from key {@value #KEY_ROOT_CONFIG}.
 * <p>
 * Configuration parameters details:
 * <ul>
 *   <li>{@value #DEFAULT_API_ROOT}: optional, you can override API root (i.e. to a self-hosted variant). Defaults to
 *   {@value #DEFAULT_API_ROOT}</li>
 *   <li>{@value #KEY_TOKEN}: required, a preview or public access token</li>
 *   <li>{@value #KEY_USE_DRAFT_VERSION}: optional, whether to use draft version, defaults to <code>false</code></li>
 *   <li>{@value #KEY_DATA_KEY}: optional, name of the key under which the loaded data from Storyblok will be put for
 *   each page. Defaults to {@value #DEFAULT_DATA_KEY}</li>
 *   <li>{@value #KEY_TARGET_NODE}: the root data node to put obtained pages under, default to root node. Value is a
 *   ordered list of path elements.</li>
 * </ul>
 */
@Getter
public class StoryblokLoaderConfig {

  private static final String KEY_ROOT_CONFIG = "storyblok-loader";
  private static final String KEY_API_ROOT = "api-root";
  private static final String KEY_TOKEN = "token";
  private static final String KEY_USE_DRAFT_VERSION = "use-draft-version";
  private static final String KEY_TARGET_NODE = "target-node";
  private static final String KEY_DATA_KEY = "data-key";

  private static final String DEFAULT_API_ROOT = "https://api.storyblok.com/v2/cdn";
  private static final String DEFAULT_DATA_KEY = "story";

  private final String apiRoot;
  private final String token;
  private final boolean useDraftVersion;
  private final String dataKey;
  private final List<String> targetNode;

  public StoryblokLoaderConfig(Config globalConfig) {
    Map<String, Object> configRoot = globalConfig.get(KEY_ROOT_CONFIG, Map.of());
    apiRoot = configRoot.getOrDefault(KEY_API_ROOT, DEFAULT_API_ROOT).toString().replaceAll("/$", "");
    token = configRoot.getOrDefault(KEY_TOKEN, "").toString();
    useDraftVersion = (Boolean) configRoot.getOrDefault(KEY_USE_DRAFT_VERSION, false);
    dataKey = configRoot.getOrDefault(KEY_DATA_KEY, DEFAULT_DATA_KEY).toString();
    targetNode = Optional.ofNullable((List<String>) configRoot.get(KEY_TARGET_NODE)).orElse(List.<String>of());

    if (token.isBlank()) {
      throw new IllegalArgumentException("Stroyblok's token can not be left empty");
    }
  }

}
