/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Currently not supported types:
 * - callbacks
 * - examples
 * - headers
 * - links
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record Components(
  Map<String, SecurityScheme> securitySchemes,
  Map<String, Parameter> parameters,
  Map<String, RequestBody> requestBodies,
  Map<String, Response> responses,
  Map<String, Schema> schemas
) {
  public Map<String, ? extends Component> getAllOfType(ComponentType componentType) {
    return switch (componentType) {
      case CALLBACKS, LINKS, HEADERS, EXAMPLES ->
        throw new RuntimeException("Unsupported type accessed: " + componentType);
      case REQUEST_BODIES -> requestBodies;
      case SECURITY_SCHEMES -> securitySchemes;
      case RESPONSES -> responses;
      case PARAMETERS -> parameters;
      case SCHEMAS -> schemas;
    };
  }

  public Components(
    Map<String, SecurityScheme> securitySchemes,
    Map<String, Parameter> parameters,
    Map<String, RequestBody> requestBodies,
    Map<String, Response> responses,
    Map<String, Schema> schemas
  ) {
    this.securitySchemes = Objects.requireNonNullElseGet(securitySchemes, Map::of);
    this.parameters = Objects.requireNonNullElseGet(parameters, Map::of);
    this.requestBodies = Objects.requireNonNullElseGet(requestBodies, Map::of);
    this.responses = Objects.requireNonNullElseGet(responses, Map::of);
    this.schemas = Objects.requireNonNullElseGet(schemas, Map::of);
  }

  public Stream<Component> streamAll() {
    return Stream
      .of(
        securitySchemes.values(),
        parameters.values(),
        requestBodies.values(),
        responses.values(),
        schemas.values()
      )
      .flatMap(Collection::stream);
  }
}
