package com.mohamnag.jstatic.plugins.openapi_processor;

public record ComponentConfig(
  String template,
  String packageName
) {

}
