package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;

public interface Component {
  ComponentType getComponentType();

  String getDescription();

  void setContext(OpenApiContext openApiContext);

  OpenApiContext getContext();

}
