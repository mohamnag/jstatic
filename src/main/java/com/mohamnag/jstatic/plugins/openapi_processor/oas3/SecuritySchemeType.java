/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SecuritySchemeType {

  HTTP("http"),
  API_KEY("apiKey"),
  AOUTH2("oauth2"),
  OPENID_CONNECT("openIdConnect"),
  ;

  private final String value;

  SecuritySchemeType(String value) {
    this.value = value;
  }

  @JsonValue
  @Override
  public String toString() {
    return this.value;
  }

  @JsonCreator
  public static SecuritySchemeType fromString(String value) {
    for (SecuritySchemeType type : SecuritySchemeType.values()) {
      if (type.value.equals(value)) {
        return type;
      }
    }

    return null;
  }

}
