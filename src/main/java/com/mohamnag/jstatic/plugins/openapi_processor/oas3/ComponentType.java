/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Optional;

/**
 * All possible component types and their related names as expected in OpenAPI spec.
 */
public enum ComponentType {
  CALLBACKS("callbacks"),
  EXAMPLES("examples"),
  HEADERS("headers"),
  LINKS("links"),
  REQUEST_BODIES("requestBodies"),
  SECURITY_SCHEMES("securitySchemes"),
  RESPONSES("responses"),
  PARAMETERS("parameters"),
  SCHEMAS("schemas"),
  ;


  /**
   * Component key as used in YAML format of OpenAPI
   */
  private final String specKey;

  ComponentType(String specKey) {
    this.specKey = specKey;
  }

  @JsonValue
  public String getSpecKey() {
    return specKey;
  }

  public static Optional<ComponentType> from(String specKey) {
    for (ComponentType value : values()) {
      if (value.specKey.equalsIgnoreCase(specKey)) {
        return Optional.of(value);
      }
    }

    return Optional.empty();
  }
}
