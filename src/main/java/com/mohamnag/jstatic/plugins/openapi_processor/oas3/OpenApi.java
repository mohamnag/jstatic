/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.beans.ConstructorProperties;
import java.util.List;
import java.util.Map;

@Slf4j
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
@EqualsAndHashCode
public class OpenApi {

  private final String openapi;
  private final Info info;
  private final List<Server> servers;
  private final Map<String, Path> paths;
  private final Components components;

  @ConstructorProperties({"openapi", "info", "servers", "paths", "components",})
  public OpenApi(
    String openapi,
    Info info,
    List<Server> servers,
    Map<String, Path> paths,
    Components components
  ) {
    this.openapi = openapi;
    this.info = info;
    this.servers = servers;
    this.paths = paths == null ? Map.of() : paths;
    this.components = components == null ?
      new Components(Map.of(), Map.of(), Map.of(), Map.of(), Map.of()) :
      components;
  }

  public void setContext(OpenApiContext context) {
    if (components != null) {
      components
        .streamAll()
        .forEach(component -> component.setContext(context));
    }

    if (paths != null) {
      paths
        .values()
        .forEach(path -> path.setContext(context));
    }
  }

}
