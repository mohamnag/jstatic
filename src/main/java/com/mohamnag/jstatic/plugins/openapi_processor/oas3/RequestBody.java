/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false, exclude = {"context"})
@AllArgsConstructor
public class RequestBody implements Component {

  private final ComponentType componentType = ComponentType.REQUEST_BODIES;
  @JsonProperty("$ref")
  private final ComponentReference ref;
  private final boolean required = false;
  private final Map<String, RequestOrResponseContent> content;
  private final String description;
  private OpenApiContext context;

  @Override
  public void setContext(OpenApiContext context) {
    this.context = context;
    if (this.content != null) {
      this.content.values().forEach(content -> content.getSchema().setContext(context));
    }
  }
}
