/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ParameterLocation {

  PATH,
  QUERY,
  HEADER,
  COOKIE,
  ;

  @JsonValue
  @Override
  public String toString() {
    return this.name().toLowerCase();
  }

  @JsonCreator
  public static ParameterLocation fromString(String string) {
    for (ParameterLocation dataType : ParameterLocation.values()) {
      if (dataType.toString().equals(string)) {
        return dataType;
      }
    }

    return null;
  }

}
