package com.mohamnag.jstatic.plugins.openapi_processor;

import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.ComponentType;

import java.util.Map;

/**
 * Will parse config with root key {@value KEY_ROOT_CONFIG}.
 * <p/>
 * To override defaults for each component type, define a sub-key with name of that component type as used in OpenAPI
 * spec. For each component type a <code>"template"</code> and a <code>"package"</code> key might be configured.
 * See {@link OpenApiProcessor} docs to find out how these values are used.
 * <p/>
 * For example use following to configure template and package for request bodies only:
 * <code><pre>
 * {@value KEY_ROOT_CONFIG}:
 *    requestBodies:
 *      template: openapi/req_body
 *      package: dtos.request_bodies
 * </pre></code>
 */
public record OpenApiProcessorConfig(
  Map<ComponentType, ComponentConfig> components
) {
  private static final String KEY_ROOT_CONFIG = "open-api-processor";

  public static OpenApiProcessorConfig extract(Config globalConfig) {
    return globalConfig.extract(KEY_ROOT_CONFIG, OpenApiProcessorConfig.class);
  }

  public String getPackageName(ComponentType componentType) {
    return this.components.getOrDefault(componentType, getDefault(componentType)).packageName();
  }

  public String getTemplate(ComponentType componentType) {
    return this.components.getOrDefault(componentType, getDefault(componentType)).template();
  }

  private ComponentConfig getDefault(ComponentType componentType) {
    return switch (componentType) {
      case CALLBACKS -> new ComponentConfig("openapi/callback", "callbacks");
      case EXAMPLES -> new ComponentConfig("openapi/example", "examples");
      case HEADERS -> new ComponentConfig("openapi/header", "headers");
      case LINKS -> new ComponentConfig("openapi/link", "links");
      case SECURITY_SCHEMES -> new ComponentConfig("openapi/security_scheme", "security_schemes");
      case PARAMETERS -> new ComponentConfig("openapi/parameter", "parameters");
      case REQUEST_BODIES -> new ComponentConfig("openapi/request_body", "request_bodies");
      case RESPONSES -> new ComponentConfig("openapi/response", "responses");
      case SCHEMAS -> new ComponentConfig("openapi/schema", "schemas");
    };
  }
}
