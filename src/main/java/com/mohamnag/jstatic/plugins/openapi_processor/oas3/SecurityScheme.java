package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false, exclude = {"context"})
@AllArgsConstructor
public class SecurityScheme implements Component {

  private final ComponentType componentType = ComponentType.SECURITY_SCHEMES;
  private final SecuritySchemeType type;
  private final SecuritySchemeScheme scheme;
  private final String bearerFormat;
  private final String description;
  private OpenApiContext context;

  @Override
  public void setContext(OpenApiContext context) {
    this.context = context;
  }
}
