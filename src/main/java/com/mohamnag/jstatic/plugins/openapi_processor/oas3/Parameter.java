/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false, exclude = {"context"})
@AllArgsConstructor
public class Parameter implements Component {

  private final ComponentType componentType = ComponentType.PARAMETERS;
  @JsonProperty("$ref")
  private final ComponentReference ref;
  private final String name;
  private final ParameterLocation in;
  private final Schema schema;
  private final boolean required = false;
  private final String description;
  private OpenApiContext context;

  public void setContext(OpenApiContext context) {
    this.context = context;

    if (this.schema != null) {
      this.schema.setContext(context);
    }
  }
}
