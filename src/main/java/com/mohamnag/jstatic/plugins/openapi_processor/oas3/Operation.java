/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class Operation {

  private final String summary;
  private final String description;
  private final String operationId;
  private final Map<String, Response> responses;
  private final List<Parameter> parameters;
  private final RequestBody requestBody;
  private final Map<String, Object> extensions = new HashMap<>();
  private OpenApiContext openApiContext;

  @JsonAnySetter
  public void addExtension(String name, Object value) {
    log.debug("Discovered extension key {} with value {}", name, value);
    extensions.put(name, value);
  }

  public Object getExtension(String name) {
    return extensions.get(name);
  }

  public void setContext(OpenApiContext openApiContext) {
    this.openApiContext = openApiContext;

    this.responses.values().forEach(response -> response.setContext(openApiContext));
    if (this.parameters != null) {
      this.parameters.forEach(parameter -> parameter.setContext(openApiContext));
    }

    if (this.requestBody != null) {
      this.requestBody.setContext(openApiContext);
    }
  }
}
