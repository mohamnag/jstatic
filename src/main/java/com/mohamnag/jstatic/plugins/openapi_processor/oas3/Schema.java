/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@ToString
@EqualsAndHashCode(callSuper = false, exclude = {"context"})
@AllArgsConstructor
public class Schema implements Component {

  private final ComponentType componentType = ComponentType.SCHEMAS;
  @JsonProperty("$ref")
  private final ComponentReference ref;
  private final DataType type;
  private final String format;
  private final Schema items;
  private final String description;
  @JsonProperty("default")
  private final String defaultValue;
  private final boolean exclusiveMinimum = false;
  private final boolean exclusiveMaximum = false;
  private final Number minimum;
  private final Number maximum;
  private final Number multipleOf;
  private final Integer minLength;
  private final Integer maxLength;
  @JsonProperty("enum")
  private final List<String> enumValues;
  private final List<String> required;
  private final Map<String, Schema> properties;
  private final List<Schema> allOf;
  private OpenApiContext context;

  public void setContext(OpenApiContext context) {
    this.context = context;
    if (this.items != null) {
      this.items.setContext(context);
    }

    if (properties != null) {
      properties.values().forEach(schema -> schema.setContext(context));
    }

    if (allOf != null) {
      allOf.forEach(schema -> schema.setContext(context));
    }
  }
}
