package com.mohamnag.jstatic.plugins.openapi_processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.data_tree.DataPage;
import com.mohamnag.jstatic.data_tree.DataTree;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.ComponentType;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.OpenApi;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Expands any page found with OpenAPI raw spec in its data, into pages under sub-nodes generated out of spec's
 * components tag (i.e. responses, request bodies, schemas, ...).
 * <p/>
 * Puts an object of type {@link OpenApiContext} in page's parent under key
 * {@value OpenApiContext#DATA_KEY_PARENT_NODE_OPEN_API_CONTEXT} in order for helpers to have access to spec's
 * information using recursive data access.
 * <p/>
 * Each component of the discovered spec will result in generation of one page. The package configured for each type of
 * component as specified by {@link OpenApiProcessorConfig#getPackageName(ComponentType)}, will be used to determine
 * which node to put each component's page under. This can even be multiple levels, for example a package config
 * <code>"dtos.request_bodies"</code> for request body components will result in a node called <code>dtos</code> as
 * sibling of where the OpenAPI data were found and a second node called <code>request_bodies</code> under it. Each
 * component of type request body will have a page under these.
 * <p/>
 * Each generated page will be added one {@link com.mohamnag.jstatic.plugins.openapi_processor.oas3.Component} under
 * key {@value PAGE_DATA_KEY_COMPONENT} which contains spec's component data. A key {@value PAGE_DATA_KEY_TEMPLATE} will
 * be also set based on component type and configured template names as per
 * {@link OpenApiProcessorConfig#getTemplate(ComponentType)} so that
 * {@link com.mohamnag.jstatic.plugins.handlebars_template_compiler.HandlebarsTemplateCompilerTask} picks up and
 * processes it.
 * <p/>
 * Currently, supports OpenAPI rawSpec v3.0.x
 */
@Slf4j
public class OpenApiProcessor implements BuildTask {

  public static final String PAGE_DATA_KEY_COMPONENT = "component";
  public static final String PAGE_DATA_KEY_TEMPLATE = "template";

  private final ObjectMapper mapper = new ObjectMapper();
  private final OpenApiProcessorConfig config;

  public OpenApiProcessor(Config globalConfig) {
    this.config = OpenApiProcessorConfig.extract(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {
    buildContext
      .getDataTree()
      .forAllPages(page -> processPage(page, buildContext.getDataTree()));
  }

  private void processPage(DataPage apiSpecPage, DataTree dataTree) {

    var openapiVersion = apiSpecPage.getData("openapi");

    if (openapiVersion.isEmpty()) {
      return;
    }

    var specVersion = openapiVersion.get().toString();
    if (!specVersion.matches("3\\.0\\.[0-9]+")) {
      log.warn("version {} is not supported, ignoring raw spec on page {}", specVersion, apiSpecPage.getPath());
      return;
    }

    log.debug("processing OpenAPI raw spec under path {} with version {}", apiSpecPage.getPath(), openapiVersion);
    OpenApi openApi = mapper.convertValue(apiSpecPage.getData(), OpenApi.class);
    apiSpecPage.getParent().addData(OpenApiContext.DATA_KEY_PARENT_NODE_OPEN_API_CONTEXT, new OpenApiContext(config, apiSpecPage, openApi, dataTree));

    expandComponentsIntoTree(config, dataTree, apiSpecPage.getParent(), openApi, ComponentType.PARAMETERS);
    expandComponentsIntoTree(config, dataTree, apiSpecPage.getParent(), openApi, ComponentType.REQUEST_BODIES);
    expandComponentsIntoTree(config, dataTree, apiSpecPage.getParent(), openApi, ComponentType.RESPONSES);
    expandComponentsIntoTree(config, dataTree, apiSpecPage.getParent(), openApi, ComponentType.SCHEMAS);
  }

  private void expandComponentsIntoTree(
    OpenApiProcessorConfig config,
    DataTree dataTree,
    DataNode apiParentNode,
    OpenApi openApi,
    ComponentType componentType
  ) {
    var packagePath = new ArrayList<>(apiParentNode.getPathElements());
    Collections.addAll(
      packagePath,
      config
        .getPackageName(componentType)
        .split("\\."));

    var templateName = config.getTemplate(componentType);

    openApi
      .getComponents()
      .getAllOfType(componentType)
      .forEach((name, component) -> {
        var componentPage = dataTree.getOrCreatePageWithPath(name, packagePath);

        componentPage.addData(PAGE_DATA_KEY_TEMPLATE, templateName);
        componentPage.addData(PAGE_DATA_KEY_COMPONENT, component);
      });
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return Collections.emptyList();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
