/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SecuritySchemeScheme {

  BASIC("basic"),
  BEARER("bearer"),
  DIGEST("digest"),
  OAUTH("OAuth"),
  ;

  private final String value;

  SecuritySchemeScheme(String value) {
    this.value = value;
  }

  @JsonValue
  @Override
  public String toString() {
    return this.value;
  }

  @JsonCreator
  public static SecuritySchemeScheme fromString(String value) {
    for (SecuritySchemeScheme type : SecuritySchemeScheme.values()) {
      if (type.value.equals(value)) {
        return type;
      }
    }

    return null;
  }


}
