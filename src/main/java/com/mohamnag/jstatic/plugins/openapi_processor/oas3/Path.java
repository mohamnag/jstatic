package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.mohamnag.jstatic.plugins.openapi_processor.OpenApiContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class Path {

  private final String summary;
  private final String description;
  private final Operation get;
  private final Operation post;
  private final Operation put;
  private final Operation delete;
  private final List<Parameter> parameters;
  private final Map<String, Object> extensions = new HashMap<>();
  private OpenApiContext openApiContext;

  @JsonAnySetter
  public void addExtension(String name, Object value) {
    log.debug("Discovered extension key {} with value {}", name, value);
    extensions.put(name, value);
  }

  public Object getExtension(String name) {
    return extensions.get(name);
  }

  public void setContext(OpenApiContext openApiContext) {
    this.openApiContext = openApiContext;

    if (this.get != null) {
      this.get.setContext(openApiContext);
    }

    if (this.post != null) {
      this.post.setContext(openApiContext);
    }

    if (this.put != null) {
      this.put.setContext(openApiContext);
    }

    if (this.delete != null) {
      this.delete.setContext(openApiContext);
    }

    if (this.parameters != null) {
      this.parameters.forEach(parameter -> parameter.setContext(openApiContext));
    }
  }
}
