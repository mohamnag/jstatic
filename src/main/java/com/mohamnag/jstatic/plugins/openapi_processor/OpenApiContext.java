package com.mohamnag.jstatic.plugins.openapi_processor;

import com.mohamnag.jstatic.data_tree.DataPage;
import com.mohamnag.jstatic.data_tree.DataTree;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.Component;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.ComponentReference;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.ComponentType;
import com.mohamnag.jstatic.plugins.openapi_processor.oas3.OpenApi;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@Getter
@ToString(exclude = {"openApi", "dataTree", "apiSpecPage"})
@EqualsAndHashCode(exclude = {"openApi", "dataTree", "apiSpecPage"})
public final class OpenApiContext {

  public static final String DATA_KEY_PARENT_NODE_OPEN_API_CONTEXT = "openApiContext";
  private final OpenApiProcessorConfig config;
  private final DataPage apiSpecPage;
  private final OpenApi openApi;
  private final DataTree dataTree;

  public OpenApiContext(OpenApiProcessorConfig config, DataPage apiSpecPage, OpenApi openApi, DataTree dataTree) {
    openApi.setContext(this);

    this.config = config;
    this.apiSpecPage = apiSpecPage;
    this.openApi = openApi;
    this.dataTree = dataTree;
  }

  @SuppressWarnings("unused") // used in templates
  public Component resolveReference(ComponentReference componentReference) {
    return componentReference.isInternal() ?
      resolveInternalReference(componentReference) :
      resolveExternalReference(componentReference);
    }

  private Component resolveInternalReference(ComponentReference componentReference) {
    String[] parts = componentReference.getRef().split("\\/");

    if (parts.length != 4) {
      log.warn("can not parse $ref string {}", componentReference);
    }

    var componentTypeOptional = ComponentType.from(parts[2]);
    if (componentTypeOptional.isEmpty()) {
      log.warn("Unknown component type: {}", parts[2]);
      return null;
    }

    var componentType = componentTypeOptional.get();
    var componentName = parts[3];

    return openApi
      .getComponents()
      .getAllOfType(componentType)
      .entrySet()
      .stream()
      .filter(stringEntry -> stringEntry.getKey().equals(componentName))
      .findFirst()
      .map(Map.Entry::getValue)
      .orElseGet(() -> {
        log.debug("could not find component with ref {}", componentReference);
        return null;
      });
  }

  private Component resolveExternalReference(ComponentReference componentReference) {
    var externalPath = componentReference.getExternalPath();
    var absolutePath = apiSpecPage.resolveRelativePath(externalPath);
    var pageOptional = dataTree.getPageWithPath(absolutePath);

    if (pageOptional.isEmpty()) {
      log.error(
        "Could not resolve path {} to external reference {} to any existing page. Make sure target page is parsed into data tree",
        absolutePath, componentReference);
      return null;
    }

    var targetPage = pageOptional.get();

    return targetPage
      .getRecursiveData(DATA_KEY_PARENT_NODE_OPEN_API_CONTEXT)
      .map(o -> o instanceof OpenApiContext openApiContext ? openApiContext : null)
      .map(openApiContext -> openApiContext.resolveReference(componentReference.getInternalPart()))
      .orElse(null);
  }

  @SuppressWarnings("unused") // used in templates
  public String getPackageName(Component component) {
    return config.getPackageName(component.getComponentType());
  }

}
