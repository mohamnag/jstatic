/*
 *  Copyright (c) 2020. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.openapi_processor.oas3;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public class ComponentReference {

  private static final String INTERNAL_REF_BEGIN = "#/components/";

  private final String ref;

  @JsonCreator
  public ComponentReference(String ref) {
    this.ref = ref;
  }

  @Override
  public String toString() {
    return this.ref;
  }

  public boolean isInternal() {
    return ref.startsWith(INTERNAL_REF_BEGIN);
  }


  /**
   * @return Path to external part of this reference relative to the spec. Will be empty list if it is an internal
   * reference.
   */
  public List<String> getExternalPath() {
    if (isInternal()) {
      return List.of();
    }

    var externalRefString = ref.substring(0, ref.indexOf(INTERNAL_REF_BEGIN));
    var pathElements = externalRefString.split("/");

    if (pathElements.length < 1) {
      return List.of();
    }

    // dropping the file extension
    var filename = pathElements[pathElements.length - 1];
    pathElements[pathElements.length - 1] = filename.substring(0, filename.lastIndexOf('.'));

    return Arrays.asList(pathElements);
  }

  /**
   * @return a {@link ComponentReference} object that contains only the internal part of reference if this is an
   * external reference. If this is internal, self object will be returned.
   */
  public ComponentReference getInternalPart() {
    if (isInternal()) {
      return this;
    }

    return new ComponentReference(ref.substring(ref.indexOf(INTERNAL_REF_BEGIN)));
  }
}
