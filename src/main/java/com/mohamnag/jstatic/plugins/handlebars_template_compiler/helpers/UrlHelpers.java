/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler.helpers;

import com.github.jknack.handlebars.Options;
import com.mohamnag.jstatic.data_tree.DataPage;

public class UrlHelpers {

  public String relativeUrl(Object context, Options options) {
    return
      context instanceof String ?
        relativizeUrl((String) context, options) :
        "";
  }

  public String removeProtocol(Object context) {
    return
      context instanceof String ?
        rmProtocol((String) context) :
        "";
  }

  private String rmProtocol(String orgUrl) {
    if (orgUrl.startsWith("https://")) {
      return orgUrl.substring(8);
    }

    if (orgUrl.startsWith("http://")) {
      return orgUrl.substring(7);
    }

    return orgUrl;
  }

  private String relativizeUrl(String orgUrl, Options options) {
    if (orgUrl.startsWith("/")) {
      orgUrl = orgUrl.substring(1);

      DataPage dataPage = (DataPage) options.context.get("page");
      int depth = dataPage.getParent().getDepth();

      return "../".repeat(depth) + orgUrl;
    }
    else if(orgUrl.isBlank()) {
      return ".";
    }
    // not starting with / means no absolute URL, no need for relativization
    else {
      return orgUrl;
    }
  }

}
