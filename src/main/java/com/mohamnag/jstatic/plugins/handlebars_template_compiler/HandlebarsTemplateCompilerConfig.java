/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler;

import com.mohamnag.jstatic.config.Config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Loads the configuration from {@value #KEY_CONFIGURATION} key and expects following sub-keys:
 *
 * <ul>
 *   <li>{@value #KEY_BASE_DIR} pointing to templates' base, this path will be prepended to all other paths and every
 *   look up is restricted to be inside this path</li>
 *   <li>{@value KEY_TEMPLATE_DATA_FIELD} defines the name of the field under which each node of data tree can specify
 *   its own template to be used for template lookup</li>
 *   <li>{@value KEY_DEFAULT_TEMPLATE} defines the default template that will be used if a node does not specify its
 *   own</li>
 *   <li>{@value KEY_TARGET_DATA_FIELD} the field's name under which the compilation result for each node of data tree
 *   will be stored</li>
 *   <li>{@value KEY_HELPERS} list of helpers that should be registered. Each entry is either the full name of a Java
 *   plugin that should be present in classpath or a path to a JavaScript file that contains helpers' code.</li>
 *   <li>{@value KEY_START_NODE} can be set to one node's path to restrict compilation to subsets of the data tree. The
 *   path should be set as a ordered list of keys that define the path.</li>
 * </ul>
 * <p>
 * For more details on the functionality see the docs on {@link HandlebarsTemplateCompilerTask} itself.
 */
public class HandlebarsTemplateCompilerConfig {

  public static final String KEY_CONFIGURATION = "handlebars-template-compiler";
  public static final String KEY_BASE_DIR = "base-dir";
  public static final String KEY_DEFAULT_TEMPLATE = "default-template";
  public static final String KEY_TARGET_DATA_FIELD = "target-data-field";
  public static final String KEY_TEMPLATE_DATA_FIELD = "template-data-field";
  public static final String KEY_HELPERS = "helpers";
  public static final String KEY_START_NODE = "start-node";

  private final Map<String, Object> configValues;

  public HandlebarsTemplateCompilerConfig(Config config) {
    this.configValues = config.get(KEY_CONFIGURATION, new HashMap<>());
    this.configValues.putIfAbsent(KEY_BASE_DIR, "/templates");
    this.configValues.putIfAbsent(KEY_DEFAULT_TEMPLATE, "default");
    this.configValues.putIfAbsent(KEY_TARGET_DATA_FIELD, "compiledHandlebars");
    this.configValues.putIfAbsent(KEY_TEMPLATE_DATA_FIELD, "template");
    this.configValues.putIfAbsent(KEY_HELPERS, List.<String>of());
    this.configValues.putIfAbsent(KEY_START_NODE, List.<String>of());
  }

  public String getBaseDir() {
    return (String) configValues.get(KEY_BASE_DIR);
  }

  public String getDefaultTemplate() {
    return (String) configValues.get(KEY_DEFAULT_TEMPLATE);
  }

  public String getTargetDataField() {
    return (String) this.configValues.get(KEY_TARGET_DATA_FIELD);
  }

  public String getTemplateDataField() {
    return (String) this.configValues.get(KEY_TEMPLATE_DATA_FIELD);
  }

  public List<String> getHelpers() {
    return (List<String>) this.configValues.get(KEY_HELPERS);
  }

  public List<String> getStartNode() {
    return (List<String>) this.configValues.get(KEY_START_NODE);
  }

}
