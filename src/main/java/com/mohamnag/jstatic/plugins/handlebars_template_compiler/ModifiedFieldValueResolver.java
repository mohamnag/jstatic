/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler;

import com.github.jknack.handlebars.context.FieldValueResolver;

import java.lang.reflect.AccessibleObject;
import java.util.Set;
import java.util.stream.Collectors;

public class ModifiedFieldValueResolver extends FieldValueResolver {

  @Override
  protected Set<FieldWrapper> members(
    Class<?> clazz) {
    var members = super.members(clazz);
    return members.stream()
      .filter(fw -> isValidField(fw))
      .collect(Collectors.toSet());
  }

  boolean isValidField(
    FieldWrapper fw) {
    if (fw instanceof AccessibleObject) {
      if (isUseSetAccessible(fw)) {
        return true;
      }
      return false;
    }
    return true;
  }

}
