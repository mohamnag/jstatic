/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler.helpers;

public class StringHelpers {

  public String removeString(Object context, String toBeRemoved) {
    return
      context instanceof String ?
        ((String) context).replace(toBeRemoved, "") :
        "";
  }

}
