/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.handlebars_template_compiler;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.io.FileTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.data_tree.DataPage;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Compiles each page's data using defined template and puts the result under {@link HandlebarsTemplateCompilerConfig#getTargetDataField()}
 * data field.
 * Template name is looked up from data field named {@link HandlebarsTemplateCompilerConfig#getTemplateDataField()} ()}
 * either on page's data or from its parent nodes all the way up using first value found. If no value is found, will
 * use default template defined in {@link HandlebarsTemplateCompilerConfig#getDefaultTemplate()}.
 * <p>
 * Templates' context has the following variables available:
 * <ul>
 *   <li><b>page</b>: which is of type {@link DataPage} and is the current page being compiled</li>
 *   <li><b>node</b>: which is of type {@link DataNode} and is the parent of current page being compiled</li>
 *   <li><b>root</b>: which is of type {@link DataNode} and refers to root node of {@link BuildContext#getDataTree()}</li>
 *   <li><b>buildContext</b>: which is of type {@link BuildContext} and is the whole build context and data</li>
 * </ul>
 * Some are just shortcuts provided for convenience.
 * <p>
 * Custom helpers might be added either as Java helpers or as JavaScript helpers.
 * <p>
 * As for Java helpers, the class name has to be passed on {@link HandlebarsTemplateCompilerConfig#getHelpers()} list.
 * For each class, all the public instance methods are registered as helpers with same name.
 * Those classes are expected to have zero arg constructor.
 * Java helpers can use SLF4J for logging. This will be then appended to default application's logs and will
 * be visible for users.
 * <p>
 * More information can be found under "Helper Source" part of
 * <a href="https://jknack.github.io/handlebars.java/helpers.html">external documentation</a>.
 * <p>
 * There is also a possibility of writing helpers in JavaScript, which is mostly suitable for cases when a helper is
 * very specific or does not make sense to be reused. JavaScript helpers can be registered by passing their file name
 * in {@link HandlebarsTemplateCompilerConfig#getHelpers()} list. Inside the JavaScript file multiple helpers can be
 * registered using following syntax:
 * <pre><code>
 *   Handlebars.registerHelper('hey', function (context) {
 *     return 'Hi ' + context.name;
 *   });
 * </code></pre>
 * <p>
 * For more info about helpers refer to <a href="https://jknack.github.io/handlebars.java/helpers.html">external
 * documentation</a>
 */
@Slf4j
public class HandlebarsTemplateCompilerTask implements BuildTask {

  private final HandlebarsTemplateCompilerConfig config;
  private final Handlebars handlebars;
  private final List<Path> jsHelperFiles = new ArrayList<>();

  public HandlebarsTemplateCompilerTask(Config globalConfig) {
    this.config = new HandlebarsTemplateCompilerConfig(globalConfig);

    TemplateLoader templateLoader =
      new FileTemplateLoader(
        config.getBaseDir()
      );

    this.handlebars = new Handlebars(templateLoader);
    this.handlebars.prettyPrint(true);
    this.handlebars.setInfiniteLoops(true);

    config
      .getHelpers()
      .forEach(source -> this.registerHelper(handlebars, source));
  }

  @Override
  public void run(BuildContext buildContext) {
    var defaultTemplateName = config.getDefaultTemplate();

    Template defaultTemplate =
      loadTemplate(defaultTemplateName, handlebars)
        .orElseThrow(() -> new RuntimeException(String.format(
          "could not load default template '%s.hbs', will not compile any handlebars templates",
          defaultTemplateName
        )));

    List<DataNode> nodes = new ArrayList<>();
    nodes.add(
      buildContext
        .getDataTree()
        .getOrCreateNodeWithPath(config.getStartNode())
    );

    while (!nodes.isEmpty()) {
      var node = nodes.remove(0);

      node
        .getPages()
        .values()
        .forEach(dataPage -> compilePage(dataPage, buildContext, handlebars, defaultTemplate, config));

      nodes.addAll(node.getSubNodes().values());
    }
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return List.of(
      Path.of(this.config.getBaseDir())
    );
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.unmodifiableList(jsHelperFiles);
  }

  private void registerHelper(Handlebars handlebars, String source) {
    boolean helperFoundOrLoaded = false;

    // first try to load it as Java class
    try {
      Class<?> helperClass = Class.forName(source);
      Object helper = helperClass.getDeclaredConstructor().newInstance();
      handlebars.registerHelpers(helper);
      helperFoundOrLoaded = true;

    } catch (ClassNotFoundException ignored) {

    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
      log.warn("helper class {} needs a zero arg constructor", source);
      helperFoundOrLoaded = true;
    }

    // if it was not found as Java class, try to load as JavaScript file
    if (!helperFoundOrLoaded) {
      File jsFile = new File(source);
      if (jsFile.exists()) {
        try {
          handlebars.registerHelpers(jsFile);
          this.jsHelperFiles.add(jsFile.toPath());
          helperFoundOrLoaded = true;

        } catch (Exception e) {
          log.debug("error registering javascript file {}", source, e);
        }
      }
    }

    if (!helperFoundOrLoaded) {
      log.warn(
        "specified handler {} could neither be loaded as Java nor JavaScript. make sure that class is in java classpath or the file path is correct",
        source
      );
    }

  }

  private Optional<Template> loadTemplate(String templateName, Handlebars handlebars) {
    Template template = null;

    try {
      template = handlebars.compile(templateName);
    } catch (IOException e) {
      log.warn("could not load template with name {}", templateName, e);
    }

    return Optional.ofNullable(template);
  }

  private void compilePage(
    DataPage dataPage,
    BuildContext buildContext,
    Handlebars handlebars,
    Template defaultTemplate,
    HandlebarsTemplateCompilerConfig config
  ) {
    try {
      //  - find out its template name
      //  - compile template using context
      Context context = Context
        .newBuilder(Map.of(
          "page", dataPage,
          "node", dataPage.getParent(),
          "root", buildContext.getDataTree().getRootNode(),
          "buildContext", buildContext
        ))
        .resolver(
          new ModifiedFieldValueResolver(),
          JavaBeanValueResolver.INSTANCE
        )
        .build();

      String compilationResult =
        dataPage
          .getRecursiveData(config.getTemplateDataField())
          .map(Object::toString)
          .flatMap(s -> loadTemplate(s, handlebars))
          .orElseGet(() -> {
            log.debug(
              "page '{}' has no field '{}' defined or it could not be loaded, using default template",
              dataPage.getPath(),
              config.getTemplateDataField()
            );

            return defaultTemplate;
          })
          .apply(context);

      //  - put back compilation result into the page
      dataPage.addData(config.getTargetDataField(), compilationResult);

    } catch (Exception e) {
      log.warn("could not compile page '{}', this page will be skipped", dataPage.getPath(), e);
    }
  }

}
