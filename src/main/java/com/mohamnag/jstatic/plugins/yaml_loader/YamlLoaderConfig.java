/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.yaml_loader;

import com.mohamnag.jstatic.config.Config;

import java.util.HashMap;
import java.util.Map;

public class YamlLoaderConfig {

  public static final String KEY_ROOT_CONFIG = "yaml-loader";
  public static final String KEY_DATA_DIR = "data-dir";

  private final Map<String, String> config;

  public YamlLoaderConfig(Config globalConfig) {
    config = globalConfig.get(KEY_ROOT_CONFIG, new HashMap<>());
    config.putIfAbsent(KEY_DATA_DIR, "/data");
  }

  public String getDataDir() {
    return config.get(KEY_DATA_DIR);
  }

}
