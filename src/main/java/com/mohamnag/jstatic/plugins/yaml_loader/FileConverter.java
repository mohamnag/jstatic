/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.yaml_loader;

import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Map;

@Slf4j
public class FileConverter {

  public static final String FILE_NAME_DATA_YAML = "data.yaml";
  public static final String ACCEPTED_FILE_EXTENSION = ".yaml";

  private static final PathMatcher pathMatcher =
    FileSystems
      .getDefault()
      .getPathMatcher("glob:**/*" + ACCEPTED_FILE_EXTENSION);

  private final Yaml reader = new Yaml();

  public boolean accepts(Path path) {
    return Files.isRegularFile(path) && pathMatcher.matches(path);
  }

  public Map<String, Object> read(Path filePath) {
    try {
      FileInputStream inputStream = new FileInputStream(filePath.toFile());
      return reader.load(inputStream);

    } catch (FileNotFoundException e) {
      log.warn("could not load YAML data file {}", filePath);
    }

    return Map.of();
  }

}
