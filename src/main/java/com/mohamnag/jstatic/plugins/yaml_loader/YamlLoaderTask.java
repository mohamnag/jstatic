/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.yaml_loader;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.plugins.AbstractDirectoryCrawler;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Loads data from YAML files into {@link com.mohamnag.jstatic.data_tree.DataTree}.
 * On each directory, if a file named {@value FileConverter#FILE_NAME_DATA_YAML} is found, it will be
 * loaded and put into {@link DataNode#getData()} with same path on data tree.
 * <p>
 * All other files with extension {@value FileConverter#ACCEPTED_FILE_EXTENSION}, will be put as {@link com.mohamnag.jstatic.data_tree.DataPage}
 * into the tree. If a page already exists with that path, data will be merged.
 */
@Slf4j
public class YamlLoaderTask extends AbstractDirectoryCrawler implements BuildTask {

  private final YamlLoaderConfig config;

  public YamlLoaderTask(Config globalConfig) {
    this.config = new YamlLoaderConfig(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {
    var rootPath = Path.of(config.getDataDir());
    var fileConverter = new FileConverter();

    try (
      var dirStream = Files.walk(rootPath)
    ) {
      dirStream
        .filter(fileConverter::accepts)
        .forEach(filePath -> {
          log.debug("loading YAML data from file {}", filePath);

          // first read data from file
          Map<String, Object> data = fileConverter.read(filePath);

          // then create/find node and add data to it
          if (filePath.getFileName().toString().endsWith(FileConverter.FILE_NAME_DATA_YAML)) {
            var dataNode = buildContext
              .getDataTree()
              .getOrCreateNodeWithPath(extractPathElements(rootPath, filePath));

            dataNode.addAllData(data);
            log.debug("total of {} data entries loaded under node {}", data.size(), dataNode.getPath());

          } else {
            var dataPage = buildContext
              .getDataTree()
              .getOrCreatePageWithPath(
                getPageName(filePath, FileConverter.ACCEPTED_FILE_EXTENSION),
                extractPathElements(rootPath, filePath)
              );

            dataPage.addAllData(data);
            log.debug("total of {} data entries loaded under page {}", data.size(), dataPage.getPath());
          }
        });

    } catch (IOException e) {
      log.warn("could not load YAML data from {}", rootPath, e);
    }

  }

  @Override
  public List<Path> underWatchInputPaths() {
    return List.of(
      Path.of(config.getDataDir())
    );
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
