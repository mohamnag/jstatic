/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.front_matter_loader;

import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

@Slf4j
class FileConverter {

  private static final Pattern TRIMS_PATTERN = Pattern.compile("^[-]{3,}");
  public static final String FILE_EXTENSION = ".md";
  private static final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:**" + FILE_EXTENSION);

  boolean accepts(Path entry) {
    return Files.isRegularFile(entry) && pathMatcher.matches(entry);
  }

  Map<String, Object> readDataFromFile(Path filePath) {
    Map<String, Object> fmData = Collections.emptyMap();

    try {
      Iterator<String> linesIterator = Files.readAllLines(filePath).iterator();
      skipTrimLine(linesIterator);
      fmData = parseFrontMatter(linesIterator);

      fmData.put(
        MarkdownFrontMatterLoaderTask.DATA_KEY_BODY,
        getRestOfLinesAsString(linesIterator)
      );

    } catch (Exception e) {
      log.warn("could not parse file {}", filePath, e);
    }

    return fmData;
  }

  private String getRestOfLinesAsString(Iterator<String> linesIterator) {
    StringBuilder stringBuilder = new StringBuilder();
    linesIterator
      .forEachRemaining(str ->
        stringBuilder
          .append(str)
          .append("\n")
      );
    return stringBuilder.toString();
  }

  private Map<String, Object> parseFrontMatter(Iterator<String> linesIterator) {
    var stringBuilder = new StringBuilder();

    while (linesIterator.hasNext()) {
      var line = linesIterator.next();

      // if we hit trim pattern, break out
      if (TRIMS_PATTERN.matcher(line).find()) {
        break;
      }
      // add line to the buffer
      else {
        stringBuilder
          .append(line)
          .append("\n");
      }
    }

    return stringBuilder.length() == 0 ?
      new HashMap<>() :
      new Yaml().load(stringBuilder.toString());
  }

  private void skipTrimLine(Iterator<String> linesIterator) {
    while (linesIterator.hasNext()) {
      var line = linesIterator.next();

      // if line is NOT blank, it should match trims
      if (!line.isBlank()) {
        if (TRIMS_PATTERN.matcher(line).find()) {
          break;
        }
        // if does not match ~> error
        else {
          throw new RuntimeException("a front matter file should start with a line of ---");
        }
      }
      // if line is blank, ignore it
    }
  }

}
