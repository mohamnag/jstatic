package com.mohamnag.jstatic.plugins.front_matter_loader;

import com.mohamnag.jstatic.config.Config;

import java.util.HashMap;
import java.util.Map;

/**
 * Configuration is read from root key {@value #KEY_ROOT_CONFIG}.
 * <p>
 * the directory from which files are read can be configured by setting key
 * {@value #KEY_DATA_DIR}. It defaults to {@value #DEFAULT_DATA_DIR}.
 */
public class FrontMatterLoaderConfig {

  public static final String KEY_ROOT_CONFIG = "front-matter-loader";
  public static final String KEY_DATA_DIR = "data-dir";

  public static final String DEFAULT_DATA_DIR = "/data";

  private final Map<String, String> config;

  public FrontMatterLoaderConfig(Config globalConfig) {
    this.config = globalConfig.get(KEY_ROOT_CONFIG, new HashMap<>());
    this.config.putIfAbsent(KEY_DATA_DIR, DEFAULT_DATA_DIR);
  }

  public String getDataRoot() {
    return this.config.get(KEY_DATA_DIR);
  }

}
