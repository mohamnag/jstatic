/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.front_matter_loader;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.plugins.AbstractDirectoryCrawler;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
class DirectoryCrawler extends AbstractDirectoryCrawler {

  private final Path rootPath;
  private final BuildContext buildContext;
  private final FileConverter fileConverter;

  private DirectoryCrawler(Path rootPath, BuildContext buildContext) {
    this.rootPath = rootPath;
    this.buildContext = buildContext;
    this.fileConverter = new FileConverter();
  }

  static DirectoryCrawler initWith(String root, BuildContext buildContext) {
    return new DirectoryCrawler(Paths.get(root), buildContext);
  }

  /**
   * Starts from root and crawls down all subdirectories and adds them to the
   * {@link BuildContext#getDataTree()}.
   */
  void crawl() {
    try (
      var dirStream = Files.walk(rootPath)
    ) {
      dirStream
        .filter(fileConverter::accepts)
        .forEach(filePath -> {

          // first convert data
          var data = fileConverter.readDataFromFile(filePath);

          // create page if conversion was successful
          var page =
            buildContext
              .getDataTree()
              .getOrCreatePageWithPath(
                getPageName(filePath, FileConverter.FILE_EXTENSION),
                extractPathElements(rootPath, filePath)
              );

          // add data to page
          data.forEach(page::addData);
        });

    } catch (IOException e) {
      throw new RuntimeException("listing files failed on given path", e);
    }
  }

}
