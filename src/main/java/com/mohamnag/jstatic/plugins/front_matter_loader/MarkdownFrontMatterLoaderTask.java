package com.mohamnag.jstatic.plugins.front_matter_loader;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataNode;
import com.mohamnag.jstatic.data_tree.DataPage;
import com.mohamnag.jstatic.data_tree.DataTree;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

/**
 * Loads each markdown data file and its front matter from configured path and adds it to
 * the {@link DataTree} as a {@link DataPage}. Directories are represented as {@link DataNode}
 * but no additional data is added to them.
 * <p>
 * Front matter data are added as they are (key-value) to the {@link DataPage} and the body
 * is is added under key {@value #DATA_KEY_BODY}.
 * <p>
 * The markdown in body, is preserved as-is and might be compiled down to HTML in template
 * just like any other field by the means that template system provides.
 * <p>
 * All files with ".md" extension are considered to be front matter files with markdown
 * body.
 * <p>
 * Some aspects of this plugin can be configured as described in {@link FrontMatterLoaderConfig}.
 */
@Slf4j
public class MarkdownFrontMatterLoaderTask implements BuildTask {

  public static final String DATA_KEY_BODY = "body";

  private final FrontMatterLoaderConfig config;

  public MarkdownFrontMatterLoaderTask(Config globalConfig) {
    this.config = new FrontMatterLoaderConfig(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {
    DirectoryCrawler
      .initWith(config.getDataRoot(), buildContext)
      .crawl();
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return List.of(
      Path.of(config.getDataRoot())
    );
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
