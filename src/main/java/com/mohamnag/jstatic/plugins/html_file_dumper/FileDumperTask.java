/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.html_file_dumper;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataPage;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * For each page, dumps what is found under the {@link FileDumperConfig#getDataField()} into
 * file and directory structures following the {@link com.mohamnag.jstatic.data_tree.DataTree}
 * of {@link BuildContext}.
 */
@Slf4j
public class FileDumperTask implements BuildTask {

  private final FileDumperConfig config;

  public FileDumperTask(Config globalConfig) {
    this.config = new FileDumperConfig(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {
    buildContext
      .getDataTree()
      .forAllPages(this::storePageOnDisk);
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return Collections.emptyList();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

  private void storePageOnDisk(DataPage dataPage) {
    Path path =
      Paths.get(
        config.getOutputDir(),
        dataPage.getPath() + config.getOutputFileExtension()
      );


    dataPage
      .getData(config.getDataField())
      .ifPresentOrElse(
        o -> {
          try {
            path.getParent().toFile().mkdirs();
            Files.write(path, o.toString().getBytes());
          } catch (IOException e) {
            log.warn("could not store data for page {}", dataPage.getPath(), e);
          }
        },
        () ->
          log.debug(
            "page {} did not have any data under field {}",
            dataPage.getPath(),
            config.getDataField()
          )
      );
  }

}
