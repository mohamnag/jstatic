/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.html_file_dumper;

import com.mohamnag.jstatic.config.Config;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class FileDumperConfig {

  public static final String KEY_OUTPUT_DIR = "output-dir";
  public static final String KEY_DATA_FIELD = "data-field";
  public static final String KEY_OUTPUT_FILE_EXTENSION = "output-file-extension";

  private final Map<String, String> configValues;

  public FileDumperConfig(Config config) {
    this.configValues = config.get("file-dumper", new HashMap<>());
    this.configValues.putIfAbsent(KEY_OUTPUT_DIR, "output/");
    this.configValues.putIfAbsent(KEY_DATA_FIELD, "compiled");
    this.configValues.putIfAbsent(KEY_OUTPUT_FILE_EXTENSION, ".html");
  }

  public String getDataField() {
    return configValues.get(KEY_DATA_FIELD);
  }

  public String getOutputDir() {
    return configValues.get(KEY_OUTPUT_DIR);
  }

  public String getOutputFileExtension() {
    return configValues.get(KEY_OUTPUT_FILE_EXTENSION);
  }

}
