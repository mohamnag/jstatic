/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractDirectoryCrawler {

  protected String getPageName(Path filePath, final String extension) {
    String string = filePath.getFileName().toString();
    return string.substring(0, string.lastIndexOf(extension));
  }

  protected List<String> extractPathElements(Path rootPath, Path filePath) {
    Path filePathParent = filePath.getParent();
    if (rootPath.equals(filePathParent)) {
      return Collections.emptyList();

    } else {
      ArrayList<String> result = new ArrayList<>();

      var relativePath = rootPath.relativize(filePathParent);
      for (int i = 0; i < relativePath.getNameCount(); i++) {
        result.add(relativePath.getName(i).toString());
      }

      return Collections.unmodifiableList(result);
    }
  }

}
