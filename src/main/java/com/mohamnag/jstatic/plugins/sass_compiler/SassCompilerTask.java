/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.sass_compiler;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.sass_lang.embedded_protocol.OutputStyle;
import de.larsgrefer.sass.embedded.CompileSuccess;
import de.larsgrefer.sass.embedded.SassCompilationFailedException;
import de.larsgrefer.sass.embedded.SassCompiler;
import de.larsgrefer.sass.embedded.SassCompilerFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * Compiles all files with extension ".scss" that does not
 * start with "_" from configured source directory into configured output
 * directory.
 * <p>
 * One might choose to compress output while compiled content is written into
 * output using configuration.
 */
@Slf4j
public class SassCompilerTask implements BuildTask {

  public static final PathMatcher PATH_MATCHER =
    FileSystems
      .getDefault()
      .getPathMatcher("glob:**/[!_]*.scss");

  private final SassCompilerConfig config;

  public SassCompilerTask(Config globalConfig) {
    this.config = new SassCompilerConfig(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {
    Path sourcePath = Paths.get(config.getSourceDir());
    Path outputPath = Paths.get(config.getOutputDir());

    try (SassCompiler sassCompiler = SassCompilerFactory.bundled(); var sourceFiles = Files.walk(sourcePath)) {

      if (config.getGenerateSourceMap()) {
        sassCompiler.setGenerateSourceMaps(true);
      }

      sourceFiles
        .filter(path -> Files.isRegularFile(path) && PATH_MATCHER.matches(path))
        .forEach(inputFilePath ->
          compileFile(sassCompiler, config, sourcePath, outputPath, inputFilePath));

    } catch (IOException e) {
      log.warn("could not compile sass files", e);
    }
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return List.of(
      Path.of(config.getSourceDir())
    );
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

  private void compileFile(
    SassCompiler sassCompiler,
    SassCompilerConfig config,
    Path sourceDirPath,
    Path outputDirPath,
    Path inputFilePath
  ) {
    try {
      Path relativePath = sourceDirPath.relativize(inputFilePath);

      Path cssFilePath =
        outputDirPath
          .resolve(relativePath)
          .resolveSibling(
            relativePath
              .getFileName()
              .toString()
              .replace(".scss", ".css")
          );

      CompileSuccess compileSuccess = sassCompiler.compileFile(
        inputFilePath.toFile(),
        // TODO: 15.12.19 output style to come from config too
        OutputStyle.EXPANDED

      );

      cssFilePath.getParent().toFile().mkdirs();
      Files.writeString(cssFilePath, compileSuccess.getCss());

      String sourceMap;
      if ((sourceMap = compileSuccess.getSourceMap()) != null) {
        Files.writeString(
          cssFilePath.resolveSibling(cssFilePath.getFileName() + ".map"),
          sourceMap);
      }

    } catch (IOException | SassCompilationFailedException e) {
      log.warn("could not compile sass file {} due to following error", inputFilePath, e);
    }
  }

}
