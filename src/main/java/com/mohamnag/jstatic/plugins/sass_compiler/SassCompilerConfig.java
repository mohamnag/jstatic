/*
 *
 *  * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *  *
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *
 *
 */

package com.mohamnag.jstatic.plugins.sass_compiler;

import com.mohamnag.jstatic.config.Config;

import java.util.HashMap;
import java.util.Map;

public class SassCompilerConfig {

  public static final String KEY_SOURCE_DIR = "source-dir";
  public static final String KEY_OUTPUT_DIR = "output-dir";
  public static final String KEY_GENERATE_SOURCE_MAP = "generate-source-map";

  private final Map<String, Object> config;

  public SassCompilerConfig(Config config) {
    this.config = config.get("sass-compiler", new HashMap<>());
    this.config.putIfAbsent(KEY_SOURCE_DIR, "sass/");
    this.config.putIfAbsent(KEY_OUTPUT_DIR, "output/css/");
    this.config.putIfAbsent(KEY_GENERATE_SOURCE_MAP, false);
  }

  public String getSourceDir() {
    return this.config.get(KEY_SOURCE_DIR).toString();
  }

  public String getOutputDir() {
    return this.config.get(KEY_OUTPUT_DIR).toString();
  }

  public boolean getGenerateSourceMap() {
    return (boolean) this.config.get(KEY_GENERATE_SOURCE_MAP);
  }

}
