/*
 *  Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.markdown_compiler;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.data_tree.DataPage;
import lombok.extern.slf4j.Slf4j;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

/**
 * This plugin compiles markdown on configured source fields of all pages down into
 * HTML and puts it back in the configured target field. Source and target fields
 * might be the same if it is wished to overwrite data.
 * <p>
 * Multiple pairs of source and target fields can be handled by this plugin.
 */
@Slf4j
public class MarkdownCompilerTask implements BuildTask {

  private final MarkdownCompilerConfig config;
  private final Parser parser;
  private final HtmlRenderer renderer;

  public MarkdownCompilerTask(Config globalConfig) {
    this.config = new MarkdownCompilerConfig(globalConfig);
    this.parser = Parser.builder().build();
    this.renderer = HtmlRenderer.builder().build();
  }

  @Override
  public void run(BuildContext buildContext) {
    buildContext
      .getDataTree()
      .forAllPages(dataPage ->
        this
          .config
          .getConversions()
          .forEach(conversion -> compile(dataPage, conversion))
      );
  }

  private void compile(DataPage dataPage, MarkdownCompilerConfig.Conversion conversion) {
    dataPage
      .getData(conversion.getSource())
      .map(Object::toString)
      .map(src -> {
        Node doc = this.parser.parse(src);
        return this.renderer.render(doc);
      })
      .ifPresentOrElse(
        rendered -> dataPage.addData(conversion.getTarget(), rendered),
        () -> log.info(
          "skipping rendering markdown for field {} into {} on page {}, no source data",
          conversion.getSource(),
          conversion.getTarget(),
          dataPage.getPath()
        )
      );
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return Collections.emptyList();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
