/*
 *  Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.markdown_compiler;

import com.mohamnag.jstatic.config.Config;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MarkdownCompilerConfig {

  public static final String KEY_SOURCE_DATA_FIELD = "sourceDataField";
  public static final String KEY_TARGET_DATA_FIELD = "targetDataField";
  private final Map<String, Object> config;
  private final List<Conversion> conversions;

  public MarkdownCompilerConfig(Config globalConfig) {
    this.config = globalConfig.get("markdown-compiler", new HashMap<>());

    conversions =
      ((List<Map<String, String>>) this.config.getOrDefault("conversions", List.of(Map.of())))
        .stream()
        .map(Conversion::new)
        .collect(Collectors.toList());
  }

  public List<Conversion> getConversions() {
    return conversions;
  }

  @Getter
  public static class Conversion {
    private final String source;
    private final String target;

    public Conversion(Map<String, String> map) {
      this.source = map.getOrDefault(KEY_SOURCE_DATA_FIELD, "mdSource");
      this.target = map.getOrDefault(KEY_TARGET_DATA_FIELD, "mdTarget");
    }
  }

}
