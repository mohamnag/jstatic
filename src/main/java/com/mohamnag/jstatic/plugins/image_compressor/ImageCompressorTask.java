/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.image_compressor;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ImageCompressorTask implements BuildTask {

    @Override
    public void run(BuildContext buildContext) {
        log.warn("image compressor run!!!");
    }

  @Override
  public List<Path> underWatchInputPaths() {
    return Collections.emptyList();
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return Collections.emptyList();
  }

}
