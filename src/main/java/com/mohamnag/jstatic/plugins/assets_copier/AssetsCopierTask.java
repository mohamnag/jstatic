/*
 *  Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.assets_copier;

import com.mohamnag.jstatic.BuildContext;
import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.config.Config;
import com.mohamnag.jstatic.plugins.AbstractDirectoryCrawler;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manages copy operations. There might be many operations configured where each operation might define one to
 * many source directories as list under key {@value AssetsCopierConfig#KEY_SOURCES} and only and only one
 * target directory under key {@value AssetsCopierConfig#KEY_TARGET}.
 * <p>
 * An optional filter can be used to filter files that are copied under key {@value AssetsCopierConfig#KEY_PATTERN}.
 * <p>
 * The name and target path of each file which is copied successfully, will be added to a node with relative path
 * to the source directory into a data key named {@value #DATA_KEY_ASSETS} as a map.
 * <p>
 * For example for a configuration of source "src/image" and target "target/assets". A file named "a.jpg" with full
 * path "src/images/some/path/a.jpg" will be copied to "target/assets/some/path/a.jpg" and node with path
 * "some/path" will have a data with key {@value #DATA_KEY_ASSETS} which is a map containing "a.jpg" ~>
 * "target/assets/some/path/a.jpg".
 * <p>
 * It is not allowed for target directory to be under any of sources. As files will overwrite the ones on target
 * and might overwrite ones from other sources based on order defined and matching name.
 */
@Slf4j
public class AssetsCopierTask extends AbstractDirectoryCrawler implements BuildTask {

  public static final String DATA_KEY_ASSETS = "assets";
  private final AssetsCopierConfig config;

  public AssetsCopierTask(Config globalConfig) {
    this.config = new AssetsCopierConfig(globalConfig);
  }

  @Override
  public void run(BuildContext buildContext) {

    this.config
      .getOperations()
      .forEach(copyOperation ->
        copyOperation
          .getSources()
          .forEach(source ->
            copyFiles(
              source,
              copyOperation.getTarget(),
              copyOperation.getPattern(),
              buildContext
            )
          )
      );
  }

  @Override
  public List<Path> underWatchInputPaths() {
    return this
      .config
      .getOperations()
      .stream()
      .flatMap(copyOperation -> copyOperation.getSources().stream())
      .map(s -> Path.of(s))
      .collect(Collectors.toList());
  }

  @Override
  public List<Path> underWatchConfigPaths() {
    return List.of();
  }

  private void copyFiles(
    String sourceDir,
    String targetDir,
    PathMatcher pattern,
    BuildContext buildContext
  ) {
    Path sourceDirPath = Paths.get(sourceDir);
    Path targetDirPath = Paths.get(targetDir);

    if (targetDirPath.startsWith(sourceDir)) {
      log.warn("target dir '{}' is under source dir '{}', will not copy any files.", targetDir, sourceDir);
      return;
    }

    try (
      var sourceFiles = Files.walk(sourceDirPath)
    ) {
      sourceFiles
        .filter(path -> Files.isRegularFile(path) && pattern.matches(path))
        .forEach(sourceFilePath ->
          copyFile(sourceFilePath, sourceDirPath, targetDirPath, buildContext));

    } catch (IOException e) {
      log.warn("could not copy files from '{}' to '{}'", sourceDir, targetDir, e);
    }
  }

  private void copyFile(
    Path sourceFilePath,
    Path sourceDirPath,
    Path targetDirPath,
    BuildContext buildContext
  ) {
    Path relativePath = sourceDirPath.relativize(sourceFilePath);
    Path targetFilePath = targetDirPath.resolve(relativePath);

    targetFilePath.getParent().toFile().mkdirs();

    try {
      Files.copy(
        sourceFilePath,
        targetFilePath,
        StandardCopyOption.REPLACE_EXISTING,
        StandardCopyOption.COPY_ATTRIBUTES
      );

      buildContext
        .getDataTree()
        .getOrCreateNodeWithPath(
          extractPathElements(sourceDirPath, sourceFilePath)
        )
        .getData(DATA_KEY_ASSETS, new HashMap<String, String>())
        .put(
          sourceFilePath.getFileName().toString(),
          targetFilePath.toString()
        );

    } catch (IOException e) {
      log.warn("could not copy file '{}' to '{}'", sourceFilePath, targetFilePath, e);
    }
  }

}
