/*
 *  Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugins.assets_copier;

import com.mohamnag.jstatic.config.Config;
import lombok.Getter;

import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AssetsCopierConfig {

  public static final String KEY_TARGET = "target";
  public static final String KEY_SOURCES = "sources";
  public static final String KEY_PATTERN = "pattern";

  private final List<CopyOperation> operations;

  public AssetsCopierConfig(Config config) {
    this.operations =
      config
        .get("assets-copier", List.of(Map.of()))
        .stream()
        .map(CopyOperation::new)
        .collect(Collectors.toList());
  }

  public List<CopyOperation> getOperations() {
    return operations;
  }

  @Getter
  public static class CopyOperation {
    private final String target;
    private final List<String> sources;
    private final PathMatcher pattern;

    public CopyOperation(Map<Object, Object> input) {
      this.target = input.getOrDefault(KEY_TARGET, "output/assets/").toString();
      this.sources = (List<String>) input.get(KEY_SOURCES);
      this.pattern =
        FileSystems
          .getDefault()
          .getPathMatcher(
            "glob:" + input.getOrDefault(KEY_PATTERN, "**/*")
          );
    }
  }

}
