/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic;

import com.mohamnag.jstatic.config.ConfigArguments;
import com.mohamnag.jstatic.file_watcher.FileWatcher;
import com.mohamnag.jstatic.file_watcher.FileWatcherArguments;
import com.mohamnag.jstatic.file_watcher.WatchedPathGroup;
import com.mohamnag.jstatic.local_server.LocalServer;
import com.mohamnag.jstatic.local_server.ServerArguments;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * This is the entry point to the application. It supports following arguments passed in on command line:
 *
 * <ul>
 *   <li><b>{@value ConfigArguments#CLI_ARG_PREFIX_CONFIG}</b>: can be used to define config file name. See
 *   {@link ConfigArguments} for details.</li>
 *
 *   <li><b>{@value FileWatcherArguments#CLI_ARG_WATCH}</b>: if passed in, will start a watch mode. In this mode
 *   specific changes will result in a rerun of pipeline or even a reload of configuration. See {@link FileWatcher}.</li>
 *
 *   <li><b>{@value ServerArguments#CLI_ARG_PREFIX_SERVE}</b>: will activate a local server to serve static files. This is
 *   independent of the watch mode. For more details see {@link ServerArguments} and {@link LocalServer} docs.</li>
 * </ul>
 */
@Slf4j
public class Application {

  private static BuildCoordinator buildCoordinator;
  private static String configFilePath;

  public static void main(String[] args) {
    var params = List.of(args);

    configFilePath =
      params
        .stream()
        .filter(ConfigArguments::canParse)
        .findFirst()
        .flatMap(ConfigArguments::parse)
        .orElseGet(ConfigArguments::defaults);

    initializePipeline();
    runPipeline();

    params
      .stream()
      .filter(ServerArguments::canParse)
      .findFirst()
      .map(ServerArguments::parse)
      .ifPresent(arguments -> new LocalServer(arguments, Application::runPipeline).start());

    params
      .stream()
      .filter(FileWatcherArguments::canParse)
      .findAny()
      .ifPresent(s -> {
        // TODO: 23.11.23 in watch mode, in addition to watching files, plugins might also internally trigger a rebuild.
        //  like by pulling an API. how to handle that?

        new FileWatcher(
          WatchedPathGroup.of(
          buildCoordinator.getUnderWatchInputPaths(),
            Application::runPipeline
          ),
          WatchedPathGroup.of(
            buildCoordinator.getUnderWatchConfigPaths(),
            () -> {
            initializePipeline();
            runPipeline();
          }
          )
        )
          .start();
      });

  }

  private static void initializePipeline() {
    log.info("initializing context");
    buildCoordinator = new BuildCoordinator(configFilePath);
  }

  private static void runPipeline() {
    log.info("running pipeline");
    buildCoordinator.run();
    log.info("finished pipeline");
  }

}
