/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.file_watcher;

import java.util.Objects;

public class FileWatcherArguments {
  public static final String CLI_ARG_WATCH = "--watch";

  public static boolean canParse(String argument) {
    return Objects.equals(argument, CLI_ARG_WATCH);
  }
}
