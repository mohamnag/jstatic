/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.file_watcher;

import com.mohamnag.jstatic.PathChangeWatch;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.FileSystems;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
public class FileWatcher {

  private final List<WatchedPathGroup> watchedPathGroups;

  /**
   * Watches changes to paths of any group and will trigger the change handler when a change is detected.
   */
  public FileWatcher(WatchedPathGroup... watchedPathGroups) {
    this.watchedPathGroups = List.of(watchedPathGroups);
  }

  public void start() {
    {
      log.info("starting watch mode, press Ctrl+C to exit");

      try {

        var watchList = new ArrayList<PathChangeWatch>();
        for (WatchedPathGroup watchedPathGroup : watchedPathGroups) {
          WatchService watchService = FileSystems.getDefault().newWatchService();

          var watch = new PathChangeWatch(
            watchService,
            watchedPathGroup.paths(),
            (watchEvents) -> watchedPathGroup.onChangeDetected().run()
          );

          watchList.add(watch);
        }

        while (true) {
          for (PathChangeWatch watch : watchList) {
            watch.poll(200, TimeUnit.MILLISECONDS);
          }
        }

      } catch (Exception e) {
        log.warn("could not start watch service", e);
      }
    }
  }
}
