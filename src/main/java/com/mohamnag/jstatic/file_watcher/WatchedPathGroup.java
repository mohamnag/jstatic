/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.file_watcher;

import java.nio.file.Path;
import java.util.List;

public record WatchedPathGroup(List<Path> paths, Runnable onChangeDetected) {
  public static WatchedPathGroup of(List<Path> paths, Runnable onChangeDetected) {
    return new WatchedPathGroup(paths, onChangeDetected);
  }
}
