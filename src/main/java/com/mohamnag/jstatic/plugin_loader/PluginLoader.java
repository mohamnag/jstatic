/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.plugin_loader;

import com.mohamnag.jstatic.BuildTask;
import com.mohamnag.jstatic.Phase;
import com.mohamnag.jstatic.config.Config;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Getter
public class PluginLoader {

  private final PluginLoaderConfig config;
  private final Config globalConfig;
  private Map<Phase, List<BuildTask>> loadedPlugins;
  private List<Path> underWatchInputPaths;
  private List<Path> underWatchConfigPaths;

  public PluginLoader(Config globalConfig) {
    this.config = new PluginLoaderConfig(globalConfig);
    this.globalConfig = globalConfig;

    this.loadPlugins();

    this.loadedPlugins = Collections.unmodifiableMap(this.loadedPlugins);
    this.underWatchInputPaths = Collections.unmodifiableList(this.underWatchInputPaths);
    this.underWatchConfigPaths = Collections.unmodifiableList(this.underWatchConfigPaths);
  }

  private void loadPlugins() {
    loadedPlugins = new HashMap<>();
    underWatchInputPaths = new ArrayList<>();
    underWatchConfigPaths = new ArrayList<>();

    for (Phase phase : Phase.values()) {
      List<BuildTask> phaseTasks =
        config
          .getClassNamesForPhase(phase)
          .stream()
          .map(this::tryLoadingPlugin)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .peek(buildTask -> {
            // TODO we could have grouped watch paths per phase so that on change, we just run that phase onward
            underWatchInputPaths
              .addAll(
                buildTask.underWatchInputPaths()
              );

            underWatchConfigPaths
              .addAll(
                buildTask.underWatchConfigPaths()
              );
          })
          .collect(Collectors.toUnmodifiableList());

      loadedPlugins.put(phase, phaseTasks);
    }
  }

  private Optional<BuildTask> tryLoadingPlugin(String className) {
    BuildTask buildTask = null;

    try {
      Class<?> pluginClass = Class.forName(className);

      Optional<?> instance = instantiateConfigCtor(pluginClass);
      if (instance.isEmpty()) {
        instance = instantiateNoArgsCtor(pluginClass);

        if (instance.isEmpty()) {
          throw new RuntimeException(String.format(
            "could not instantiate plugin %s with config constructor or no-args constructor",
            className
          ));
        }
      }

      Object newInstance = instance.get();

      if (newInstance instanceof BuildTask) {
        buildTask = ((BuildTask) newInstance);
        log.debug("successfully loaded plugin {}", className);

      } else {
        log.warn("class {} can not be used, it should implement {}", className, BuildTask.class.getName());
      }

    } catch (Exception e) {
      log.warn("could not load class {}, ignoring this plugin", className, e);
    }

    return Optional.ofNullable(buildTask);
  }

  private <T> Optional<T> instantiateNoArgsCtor(Class<T> pluginClass) {

    T newInstance = null;
    try {
      newInstance =
        pluginClass
          .getConstructor()
          .newInstance();

    } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
      log.warn("failed instantiating no args constructor on plugin {}", pluginClass.getName(), e);

    } catch (NoSuchMethodException e) {
      log.debug("no args constructor not found on plugin {}", pluginClass.getName());
    }

    return Optional.ofNullable(newInstance);
  }

  private <T> Optional<T> instantiateConfigCtor(Class<T> pluginClass) {

    T newInstance = null;
    try {
      newInstance =
        pluginClass
          .getConstructor(Config.class)
          .newInstance(globalConfig);

    } catch (NoSuchMethodException e) {
      log.debug("config constructor not found on plugin {}", pluginClass.getName());

    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      log.warn("failed instantiating config constructor on plugin {}", pluginClass.getName(), e);
    }

    return Optional.ofNullable(newInstance);
  }

}
