package com.mohamnag.jstatic.plugin_loader;

import com.mohamnag.jstatic.Phase;
import com.mohamnag.jstatic.config.Config;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PluginLoaderConfig {

  public static final String ROOT_CONFIG_KEY = "plugins";

  private final Map<String, List<String>> pluginClassNames;

  public PluginLoaderConfig(Config config) {
    this.pluginClassNames = config.get(ROOT_CONFIG_KEY, new HashMap<>());

    Arrays.stream(Phase.values())
      .forEach(phase ->
        this.pluginClassNames.computeIfAbsent(
          phase.name(),
          s -> Collections.emptyList()
        ));
  }

  public List<String> getClassNamesForPhase(Phase phase) {
    return pluginClassNames.get(phase.name());
  }

}
