/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

@Slf4j
@Getter
public class Config {

  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
    .setPropertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE);

  private final Map<String, Object> data;

  Config(Map<String, Object> data) {
    this.data = data;
  }

  public <V> V extract(String rootKey, Class<V> configClass) {
    return OBJECT_MAPPER.convertValue(
      data.getOrDefault(rootKey, Map.of()),
      configClass
    );
  }

  @SuppressWarnings("unchecked")
  public <T> List<T> get(String key, List<T> defaultValue) {
    Object value = data.get(key);

    if (value == null) {
      log.debug("no value found for key {}, returning default", key);
      return defaultValue;

    } else if (value instanceof List) {
      return (List<T>) value;

    } else {
      log.debug("value for key {} has different type than requested, returning default", key);
      return defaultValue;
    }
  }

  @SuppressWarnings("unchecked")
  public <K, V> Map<K, V> get(String key, Map<K, V> defaultValue) {
    Object value = data.get(key);

    if (value == null) {
      log.debug("no value found for key {}, returning default", key);
      return defaultValue;

    } else if (value instanceof Map) {
      return (Map<K, V>) value;

    } else {
      log.debug("value for key {} has different type than requested, returning default", key);
      return defaultValue;
    }
  }
}
