/*
 *  Copyright (c) 2023. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.config;

import java.util.Optional;

/**
 * Accepts an argument starting with {@value CLI_ARG_PREFIX_CONFIG} followed by the path to the config file like
 * following:
 * <code>{@value CLI_ARG_PREFIX_CONFIG}=path/to/config.yaml</code>
 * <p>
 * If not provided, file name will default to {@value DEFAULT_CONFIG_FILE_NAME} in current directory.
 */
public class ConfigArguments {
  public static final String CLI_ARG_PREFIX_CONFIG = "--config";
  public static final String DEFAULT_CONFIG_FILE_NAME = "config.yaml";

  public static boolean canParse(String s) {
    return s.startsWith(CLI_ARG_PREFIX_CONFIG + "=");
  }

  public static Optional<String> parse(String s) {
    var replace = s.replace(CLI_ARG_PREFIX_CONFIG + "=", "");

    if (replace.isBlank()) {
      return Optional.empty();
    }

    return Optional.of(replace);
  }

  public static String defaults() {
    return DEFAULT_CONFIG_FILE_NAME;
  }
}
