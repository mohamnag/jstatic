/*
 * Copyright (c) 2019. Mohammad Naghavi <mohamnag@gmail.com> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package com.mohamnag.jstatic.config;

import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class ConfigLoader {

  public static Config load(String configFilePath) {
    Optional<InputStream> externalConfigFile = openExternalConfigFile(configFilePath);
    if (externalConfigFile.isPresent()) {
      log.info("using external config file {}", configFilePath);
      return new Config(
        new Yaml()
          .load(externalConfigFile.get())
      );

    } else {
      log.warn("no custom config file found, using default config values");
      return new Config(Map.of());
    }

  }

  private static Optional<InputStream> openExternalConfigFile(String configFilePath) {
    File externalConfigFile = new File(configFilePath);
    InputStream configInputStream = null;

    try {
      configInputStream = new FileInputStream(externalConfigFile);
    } catch (FileNotFoundException e) {
      log.trace("external config could not be opened", e);
    }

    return Optional.ofNullable(configInputStream);
  }
}
