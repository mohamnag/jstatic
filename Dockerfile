FROM openjdk:21-slim

ENV WORKDIR '/jstatic/home/'
ENV PATH /jstatic/bin:$PATH

VOLUME $WORKDIR
WORKDIR $WORKDIR

COPY build/install/jstatic/ /jstatic/

ENTRYPOINT ["/jstatic/bin/jstatic"]
