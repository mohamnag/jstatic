# JStatic Website

This folder is where the webpage and documentation of the project lives. It is set up in a way to give you a complete and independent example of how you would use JStatic for generating an static website using JStatic.

Browse files to understand the setup or just download and copy the whole directory to change and use as starting point for your own website.

## Overview of the Setup

This is a very high level overview of the setup:

- it uses the docker image of JStatic to do the build
- a config file in default location configures the JStatic pipeline, generation's output goes into `output` directory
- a simple log configuration gives info about build process
- whole setup is configured to run on GitLab CI

This is how JStatic is configured:

- all site data is located under `data` directory
- static assets are located under `assets` directory
- `style` directory contains style files that are processed down to plain CSS later
- `templates` directory contains all Handlebars templates


### Styles

We use tailwindcss here. The CSS needs to be generated using [its own CLI](https://tailwindcss.com/blog/standalone-cli). 

For dev purposes:
```shell
cd style
tailwindcss -i input.css -o main.css --watch
```

For final generation before committing the file:
```shell
cd style
tailwindcss -i input.css -o main.css --minify
```


### Styles

We use tailwindcss here. The CSS needs to be generated using [its own CLI](https://tailwindcss.com/blog/standalone-cli). 

For dev purposes:
```shell
cd style
tailwindcss -i input.css -o main.css --watch
```

For final generation before committing the file:
```shell
cd style
tailwindcss -i input.css -o main.css --minify
```


### Styles

We use tailwindcss here. The CSS needs to be generated using [its own CLI](https://tailwindcss.com/blog/standalone-cli). 

For dev purposes:
```shell
cd style
tailwindcss -i input.css -o main.css --watch
```

For final generation before committing the file:
```shell
cd style
tailwindcss -i input.css -o main.css --minify
```
