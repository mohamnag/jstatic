---
title: Documentation
---

> The best and most up-to-date place to get ideas about how and what can be done with JStatic is the [source code of the tests](https://gitlab.com/mohamnag/jstatic/-/tree/master/src/test). So make sure to have a look there when you can't find answers to your questions here in these docs. 

To understand basics and get started:
- [Basic Concepts](basic-concepts.html)
- [How to install](installation.html)
- [How to use watch mode or integrated server](watch-mode-and-local-server.html)
- [How to configure](configurations.html)


Guides and more advanced how-tos:
- [Setup as a Gradle task](setup-gradle-task.html)
