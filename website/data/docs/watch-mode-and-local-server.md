---
title: Watch mode & Local Server
---

To improve developer usability, following independent features are provided. They can be used standalone or in combination to speed up your workflow.

## Watch mode

Watch mode can be activated by passing the parameter `--watch` to CLI for example as: `/jstatic/bin/jstatic --watch`. While enabled, the process will not exit and will keep rebuilding the content when input or configuration files change. This will enable you to automatically rebuild output while you are changing data, templates, assets or similar.

This is still an experimental feature and might influence the performance drastically.

## Local Server

As in many cases, HTML is the desired output and having a static content webserver is very handy, JStatic also includes a simple one integrated. To activate it you have to pass `--serve=path/to/html/output` parameter to CLI. 
This will start the server and serve content of `path/to/html/output` to default port. This server is also capable of starting an HTTPS server with a self-signed certificate. 

More details about parameters and functionality see javadocs [here](/javadocs/com/mohamnag/jstatic/local_server/serverarguments) and [here](/javadocs/com/mohamnag/jstatic/local_server/localserver).
