---
title: Configurations
---

## Pipeline config

Per default the application looks for a file named `config.yaml` in current directory to set up the pipeline. If for any reason your config file shall be named differently, use the `--config` parameter as follows to run the application: `/jstatic/bin/jstatic --config=config-file-name.yaml`.

All directory and file paths defined in the config file are relative to current working directory.

Config file consists of two major parts:

- `plugins` tag
- individual plugin configurations

> To see a full list of shipped plugins and their documentation, check [this package](/javadocs/com/mohamnag/jstatic/plugins/package-summary.html) under javadocs.

### "plugins" tag

This tag is normally the first one in the config file and contains the list of plugins which should be activated for each phase. For example:

```yaml
plugins:
  LOAD_DATA_TREE:
    - com.mohamnag.jstatic.plugins.front_matter_loader.MarkdownFrontMatterLoaderTask

  PROCESS_ASSETS:
    - com.mohamnag.jstatic.plugins.sass_compiler.SassCompilerTask
    - com.mohamnag.jstatic.plugins.assets_copier.AssetsCopierTask

  COMPILE_TEMPLATES:
    - com.mohamnag.jstatic.plugins.markdown_compiler.MarkdownCompilerTask

  DUMP_TREE:
    - com.mohamnag.jstatic.plugins.html_file_dumper.FileDumperTask
```

This configuration:

- enables one plugin per phase except for `PROCESS_ASSETS` which has two plugins enabled
- the order of plugins inside one phase is essential as this dictates the order of execution and might influence the outcome. For example here the SASS compiler might generate some output that assets copier picks up and copies over.
- as you can see, there is no individual configuration per plugin here

> To see an almost complete example of all possible configurations, check the [config file](https://gitlab.com/mohamnag/jstatic/-/blob/master/src/test/resources/int-test/config.yaml) under tests.

### Individual plugin configurations

Second part of a config file is the plugin configurations. It starts with a plugin's name and then contains keys that plugin has defined for its adjustments. For the shipped plugins, you can find the necessary information on the Javadocs. For example checking `com.mohamnag.jstatic.plugins.handlebars_template_compiler.HandlebarsTemplateCompilerTask`, you will find its config class called [HandlebarsTemplateCompilerConfig](/javadocs/com/mohamnag/jstatic/plugins/handlebars_template_compiler/HandlebarsTemplateCompilerConfig.html). This class gives a short introduction to what keys are available to configure the functionality of plugin, whereas the task itself [HandlebarsTemplateCompilerTask](/javadocs/com/mohamnag/jstatic/plugins/handlebars_template_compiler/HandlebarsTemplateCompilerTask.html) gives a more detailed description of the functionality. Same pattern could be used to discover information about other shipped plugins.

## Log output config

JStatic uses logback for formatting and outputting the logs. Default config packaged with app uses standard output and does some job of formatting. However, the packaged application is also configured to use a file named `logback.xml` from current directory where it is being run from. One can just tweak the output by creating such a file following the configuration details of logback (can be found here)[http://logback.qos.ch/manual/jmxConfig.html]. Here is a simple example of such file:

```xml
<configuration>

  <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
    <encoder>
      <pattern>%highlight([%-5level]) %d{HH:mm:ss.SSS} - %msg%n%ex{0}</pattern>
    </encoder>
  </appender>

  <logger name="com.mohamnag.jstatic" level="DEBUG"/>

  <root level="WARN">
    <appender-ref ref="STDOUT"/>
  </root>

</configuration>
```
