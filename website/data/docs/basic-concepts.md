---
title: Basic Concepts
---

## Data Tree

![](/assets/datatree.svg)

JStatic defines an abstraction layer on data. All data is organised in a "data tree". This tree consists of mainly two types of elements: **nodes** and **pages**. 

Nodes have two main functionalities:
- They maintain the hierarchy
- They contain data (that is shared by belonging pages or sub-nodes and can be accessed by them)

Pages represent terminal elements of the tree (they can not have sub-elements). Their main functionality is to contain data (which is not common with other parts).

So lets say we have a bunch of blog posts, and they all have some common data (like author's information). Each blog post also contains its own data which is not common. These private, not sharable data ends up in a "page" for each post. All ths common information should end up in closes parent "node" that is the shared ancestor of all those pages or their belonging nodes.

One is allowed (and encouraged) to traverse the tree upwards recursively to obtain certain piece of data. To support this each page has a method called `getRecursiveData(String key)` which can be called passed in a key and it will traverse the tree to find first data piece with that given key. This can be used for example in a Handlebars helper to retrieve certain data. Something like:

```javascript
Handlebars.registerHelper(
    'recursiveData',
    function (context, key) {
        return context.getRecursiveData(key).orElse('');
    }
);
```

Which can then be used as:
```handlebars
{{recursiveData page 'some-key'}}
```

## Process Overview

![](/assets/diagram_flow.svg)

JStatic's executor is compromised of one orchestrator and a lot of plugins. Each of those plugins might be enabled selectively in case it is required. Each enabled plugins needs to be configured to work properly.

Each plugin will be assigned to a specific phase to run in which depends on the plugin functionality. Each plugin defines its configuration parameters which define its input and output. It will process something either from a kind of input (files, APIs, ...) or from (part of) data tree and then puts the results somewhere into the data tree or into a kind of output (files, APIs, ...).

Multiple plugins might be put together as a single pipe to derive the results you need.

Examples of existing plugins' functionality are:

- loading markdown & frontmatter files into data tree
- loading YAML files into data tree
- loading environment variables into data tree
- compiling SASS files from one directory into another directory
- compiling handlebars templates (using data inside data tree) and put results back in data tree
- dumping part of data tree (for example compilation results) from data tree to the disk

Above examples are some use-cases of some of the existing plugins and shows what is meant with piping them together and gives insight how this system is put together. You need to read the documentation of each plugin to understand details of its functionality.
