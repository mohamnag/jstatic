---
title: Installation
---

## As CLI

You can download the latest distribution version from [here](https://gitlab.com/mohamnag/jstatic). This needs Java 13 to run. After downloading it, you can symlink it to your `/bin` directory to be always accessible:

For example on macOS run following to link:

```bash
$ sudo ln -s /path/to/distribution/of/jstatic/bin/jstatic /usr/local/bin/jstatic
```

> This is right now out of the scope of this guide due to its complexity and variety for different systems.

## Use Docker Image

We publish following images:
- from all commits from any branch to [GitLab's Container Registry](https://gitlab.com/mohamnag/jstatic/container_registry):
  - snapshot versions (i.e. `registry.gitlab.com/mohamnag/jstatic:0.4.0-the-branch-name-SNAPSHOT`)
  - commit hash tags (i.e. `registry.gitlab.com/mohamnag/jstatic:78b6426bc7bd6bbde13ac563114bdfbab8e16cc8`)
- from commits on the main branch to [Docker Hub](https://hub.docker.com/r/mohamnag/jstatic):
  - release version (i.e. `mohamnag/jstatic:0.4.0`)
  - commit hash tags (i.e. `mohamnag/jstatic:78b6426bc7bd6bbde13ac563114bdfbab8e16cc8`)
  - latest tag (i.e. `mohamnag/jstatic:latest`)

Whereas:
- all image tags on GitLab Registry are only intended to be used for development purposes, and they get pruned regularly
- on Docker Hub:
  - tags with commit hash are the absolute ref to a certain version and will never be changed
  - release version tags might be replaced in certain critical cases
  - latest tag is always pointing to latest version and there is no guarantee of backwards compatibility

Decide based on your use case, which tag and from where you want to consume.

To use the docker image you should:

1. mount your data under working directory which is `/jstatic/home/`. This shall contain:
   - your own `config.yaml` file. See [default config](https://gitlab.com/mohamnag/jstatic/-/blob/master/src/test/resources/int-test/config.yaml) for possible values
   - all your other input files for data and possible plugins
2. run image
3. use output as configured in `config.yaml`

Assuming using the latest release image and having your project files in current directory, the command would look like:
```shell
$ docker run -v $(pwd):/wrk -w /wrk mohamnag/jstatic
```

You can pass in any parameter that you would pass to CLI by appending it to above. For example to start watch mode:
```shell
$ docker run -v $(pwd):/wrk -w /wrk mohamnag/jstatic --watch
```

## As Java Library

No surprise that you can include and use this library as a Java library too. Use coordinates in following sections to install the dependency.

### Releases

Releases are published to [maven central](https://search.maven.org/artifact/com.mohamnag/jstatic).

in Gradle:

```groovy
repositories {
  mavenCentral()
}

dependencies {
  implementation 'com.mohamnag:jstatic:X.Y.Z'
}
```

in Maven:

```xml

<dependency>
  <groupId>com.mohamnag</groupId>
  <artifactId>jstatic</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

### Snapshots

Snapshots are published to [OSS Sonatype snapshot repository](https://oss.sonatype.org/content/repositories/snapshots).

in Gradle:

```groovy
repositories {
  maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
}

dependencies {
  implementation 'com.mohamnag:jstatic:X.Y.Z'
}
```

in Maven (you need to add `https://oss.sonatype.org/content/repositories/snapshots` as repository in your maven settings):

```xml

<dependency>
  <groupId>com.mohamnag</groupId>
  <artifactId>jstatic</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

> Replace `X.Y.Z` with desired version.

## As Gradle Task

You can use JStatic as a Gradle task to achieve a number of goals:

- generating/compiling documentations
- generating code (for example from an OpenAPI specification)
- any other task that such a templating system can full-fill

As the setup and usage are in this case more complicated, refer to its own [dedicated page](setup-gradle-task.html) to find more and see the sample setup how JStatic has been used to generate POJOs from OpenAPI spec.
