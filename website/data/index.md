---
title: Welcome
---

[![Maven Central](https://img.shields.io/maven-central/v/com.mohamnag/jstatic)](https://search.maven.org/search?q=g:com.mohamnag%20a:jstatic)
[![Sonatype Nexus (Snapshots)](https://img.shields.io/nexus/s/com.mohamnag/jstatic?server=https%3A%2F%2Foss.sonatype.org)](https://oss.sonatype.org/#nexus-search;gav~com.mohamnag~jstatic)

JStatic is a system consisting of multiple plugins built around a templating engine ([Handlebars Java](https://jknack.github.io/handlebars.java/)) to make it possible to generate many kinds of output out of a combination of inputs and input types.

## Goals

- filesystem similar but filesystem independent, intentional use of nodes and pages to abstract away from files so that in case necessary, one can plug in other data sources and/or other targets
- multi format support for data source, currently supporting following sources/data types:
  - markdown + front matter
  - JSON files
  - YAML files
  - OpenAPI Specification
  - Environment variables
  - [Storyblok](https://www.storyblok.com/) headless CMS
- functionality to be extensible as easy as possible: using Java and JavaScript languages in form of helper scripts for helpers and/or plugins
- using a flexible yet logic-less template system, the battle-hardened [Handlebars](https://handlebarsjs.com/)
- always have access to all the data. Use data from other parts of data tree anywhere necessary, don't repeat data!
- make exceptions and special functionality as seldom as possible. everything has to go through a generic process that leaves the possibility of extension open.
- support website's development process by providing a watch mode and an integrated webserver

## Where to go next?

- [Docs and guides](docs/) gives an intro and also some usage examples and guides
- [Javadocs](javadocs/) gives you access to detailed information about the functionality and other information like list of plugins
