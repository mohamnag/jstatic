/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "../templates/**/*.{hbs,js}",
    "../data/**/*.md",
  ],
  theme: {
    extend:
      {
        fontFamily: {
          'mono': ['JetBrains Mono', 'Roboto Mono', 'ui-monospace', 'monospace']
        },
      },
  },
  plugins: [],
}

