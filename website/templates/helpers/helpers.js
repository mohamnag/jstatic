Handlebars.registerHelper(
    'eachParentOf',
    function (context, options) {
        let parent = context.parent;
        let result = '';
        let reverse = options.hash["reverse"];

        while (parent != null) {
            result = reverse ?
                options.fn(parent) + result :
                result + options.fn(parent);

            parent = parent.parent;
        }

        return result;
    }
);

Handlebars.registerHelper(
    'recursiveData',
    function (context, key) {
        return context.getRecursiveData(key).orElse('');
    }
);

Handlebars.registerHelper(
    'startsWith',
    function (context, searchStr) {
        return context.startsWith(searchStr);
    }
);

Handlebars.registerHelper(
    'eq', function (v1, v2) {
        return v1 === v2
    });

Handlebars.registerHelper(
    'ne', function (v1, v2) {
        return v1 !== v2;
    });

Handlebars.registerHelper(
    'lt', function (v1, v2) {
        return v1 < v2;
    });

Handlebars.registerHelper(
    'gt', function (v1, v2) {
        return v1 > v2;
    });

Handlebars.registerHelper(
    'lte', function (v1, v2) {
        return v1 <= v2;
    });

Handlebars.registerHelper(
    'gte', function (v1, v2) {
        return v1 >= v2;
    });

Handlebars.registerHelper(
    'and', function () {
        return Array.prototype.slice.call(arguments).every(Boolean);
    });

Handlebars.registerHelper(
    'or', function () {
        return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
    });
